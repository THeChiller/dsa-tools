import os
import re
from enum import Enum
from pprint import pp
from typing import Tuple

from pydantic import BaseModel

import yaml


class Plant(BaseModel):
    name: str
    harvest_times: list[str]
    identification: int
    zba_page: int
    amount: str
    spread: list[Tuple[str, str]]

    def __lt__(self, other):
        return self.name < other.name


class Spread(str, Enum):
    SEHRHAEUFIG = "extremeCommon"
    HAEUFIG = "common"
    GELEGENTLICH = "sometimes"
    SELTEN = "rare"
    SEHRSELTEN = "extremeRare"
    KEINE = "none"


special_cases = {
    "identification": set(),
    "amount": set(),
}
areas = {}
plants: list[Plant] = []


def split_source_file():
    with open("data_converter/raw/DSATool/Pflanzen.cs", "r",
              encoding="iso-8859-1") as source:
        lines = source.readlines()

    filename = ""
    plants_section = False
    for line in lines:
        if "# endregion" in line:
            plants_section = False
        if "#region Pflanzen" in line:
            plants_section = True
            continue
        if line == "\n":
            continue
        if plants_section:
            if "public class Pflanze" in line:
                plant_name = line.split("public class Pflanze")[1]
                plant_name = plant_name.split(":")[0]
                plant_name = plant_name.replace(" ", "")
                plant_name = re.sub(r"(\w)([A-Z])", r"\1 \2", plant_name)
                filename = plant_name.lower().replace(" ", "_") + ".cs"
                with open(f"data_converter/split/{filename}", "w") as file:
                    pass
            with open(f"data_converter/split/{filename}", "a") as file:
                file.write(line)


def convert_file(src_filename: str) -> Plant:
    plant = {
        "name": None,
        "harvest_times": [],
        "identification": None,
        "zba_page": None,
        "amount": None,
        "spread": []
    }
    with open(src_filename, "r") as file:
        lines = file.readlines()
    try:
        for line in lines:
            if line.lstrip().startswith("//"):
                continue
            if "Bestimmung" in line:
                if "GetBestimmung" in line:
                    special_cases["identification"].add(
                        src_filename.split("/")[-1].split(".")[0])
                    continue
                plant["identification"] = int(line.split("=")[1].lstrip()[:-2])
            if "Name" in line and "Namenlos" not in line:
                plant["name"] = line.split('"')[1]
            if "Erntezeit.Add" in line:
                plant["harvest_times"].append(line.split('"')[1])
            if "SeiteReferenz" in line:
                plant["zba_page"] = int(line.split("=")[1].lstrip()[:-2])
            if "Grundmenge" in line:
                if "GetGrundmenge" in line:
                    special_cases["amount"].add(
                        src_filename.split("/")[-1].split(".")[0])
                    continue
                plant["amount"] = line.split('"')[1]
            if "Verbreitung.Add" in line:
                area = line.split('"')[1]
                amount = line.split("EVorkommen")[1]
                amount = amount[1:-4]
                plant["spread"].append((area, Spread[amount].value))

        return Plant(**plant)
    except Exception as e:
        print(f"error in {src_filename}")
        print(e)


def convert_plant_files():
    src_path = "data_converter/split/"
    dst_path = "data_converter/parsed/"
    file_list = os.listdir(src_path)
    for file in file_list:
        if file == ".gitkeep":
            continue
        plants.append(convert_file(src_filename=src_path + file))


def get_plant_code(plant: Plant) -> str:
    def get_harvest_str() -> str:
        return ','.join(
            [f'AventuricMonth.{month.lower()}' for month in plant.harvest_times])

    def get_spread_str() -> str:
        return ','.join(
            [f'new Spread(GeographicArea.{areas[area]}, Rarity.{rarity})' for
             area, rarity in plant.spread])

    template = f"""
            "{plant.name}": new Plant(
                "{plant.name}",
                "context",
                "description",
                PlantType.crop,
                [AventuricArea.all],
                [
                    {get_spread_str()}
                ],
                Identification["{plant.identification*-1}"],
                [{get_harvest_str()}],
                "{plant.amount}",
                "doseAndEffect",
                new Durability(TimeFrame.days, 1, 0, DiceType.d6),
                new Price(),
                "alchimicUse",
                "specialNotes",
                [
                    new Reference(Book.zba, [{plant.zba_page}])
                ]
            ),\n
    """
    template = f"""
            "{plant.name}": new Plant(
                true,
                "{plant.name}",
                "Kontext",
                "Beschreibung",
                [PlantType.crop],
                [AventuricArea.all],
                [
                  {get_spread_str()}
                ],
                Identification["{plant.identification*-1}"],
                [
                  new PlantPart(
                    "{plant.amount}",
                    new Durability(TimeFrame.days, 1, 0, DiceType.d6),
                    new Price()
                  ),
                ],
                [
                  {get_harvest_str()}
                ],
                "Dosis",
                "Alchimistische Verwendung",
                "Besondere Bemerkungen",
                [
                  new Reference(Book.zba, [{plant.zba_page}])
                ],
                undefined,
                undefined
            ),\n
    """
    return template


def read_areas():
    with open("src/schemas/geography/geographicArea.ts", "r") as file:
        lines = file.readlines()
    for line in lines:
        if "=" not in line:
            continue
        strp_line = line.lstrip().rstrip()
        parts = strp_line.split("=")
        key = parts[0].lstrip().rstrip()
        value = parts[1].lstrip().rstrip().replace('"', "")
        if value[-1] == ",":
            value = value[:-1]
        areas[value] = key


def plant_ts_file():
    file_content = """
    import {
      AventuricArea,
      Book,
      DiceType,
      Durability,
      GeographicArea,
      Identification,
      Plant,
      AventuricMonth,
      PlantType,
      Price,
      Rarity,
      Reference,
      Spread,
      TimeFrame,
      PlantPart,
    } from "@/schemas";
    
    export function getActivePlants(): Record<string, Plant> {
      const activePlants: Record<string, Plant> = {};
      for (const plantsKey in plants) {
        if (plants[plantsKey].active) {
          console.log(plantsKey);
          activePlants[plantsKey] = plants[plantsKey];
        }
      }
      return activePlants;
    }
    
    const plants: Record<string, Plant> = {\n"""

    for plant in plants:
        plant_code = get_plant_code(plant=plant)
        file_content += plant_code

    file_content += "}"
    with open("data_converter/parsed/plants.ts", "w") as file:
        file.write(file_content)


if __name__ == "__main__":
    split_source_file()
    convert_plant_files()
    plants = sorted(plants)
    read_areas()
    plant_ts_file()
    # with open("data_converter/special_cases.yml", "w", encoding="utf-8") as file:
    #     yaml.dump(special_cases, file, encoding="utf-8", allow_unicode=True)
