import { createRouter, createWebHashHistory } from "vue-router";

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("../views/public/Home.vue"),
    },
    {
      path: "/tools/alchimische-produkte/suche",
      name: "alchimicProducsSearch",
      component: () =>
        import("../views/public/tools/alchimicProducts/Search.vue"),
    },
    {
      path: "/tools/alchimische-produkte/brauen",
      name: "alchimicProducsBrewing",
      component: () =>
        import("../views/public/tools/alchimicProducts/Brewing.vue"),
    },
    {
      path: "/tools/pflanzen/suche",
      name: "plantsSearch",
      component: () => import("../views/public/tools/plants/Search.vue"),
    },
    {
      path: "/tools/tiere/suche",
      name: "animalsSearch",
      component: () => import("../views/public/tools/animals/Search.vue"),
    },
    {
      path: "/tools/ausruestung/suche",
      name: "equipmentSearch",
      component: () => import("../views/public/tools/equipment/Search.vue"),
    },
    {
      path: "/tools/kampf/basics",
      name: "combatBasics",
      component: () => import("../views/public/tools/combat/Basics.vue"),
    },
    {
      path: "/tools/kampf/organisator",
      name: "combatOrganiser",
      component: () => import("../views/public/tools/combat/Organizer.vue"),
    },
    {
      path: "/tools/metatalente/angeln",
      name: "metaTalentsFishing",
      component: () => import("../views/public/tools/metaTalents/Fishing.vue"),
    },
    {
      path: "/tools/metatalente/nahrung-sammeln",
      name: "metaTalentsGatherFood",
      component: () =>
        import("../views/public/tools/metaTalents/GatherFood.vue"),
    },
    {
      path: "/tools/metatalente/kraeuter-sammeln",
      name: "metaTalentsGatherHerbs",
      component: () =>
        import("../views/public/tools/metaTalents/GatherHerbs.vue"),
    },
    {
      path: "/tools/metatalente/pirschjagd",
      name: "metaTalentsStalkingHunt",
      component: () =>
        import("../views/public/tools/metaTalents/StalkingHunt.vue"),
    },
    {
      path: "/tools/metatalente/ansitzjagd",
      name: "metaTalentsStationaryHunt",
      component: () =>
        import("../views/public/tools/metaTalents/StationaryHunt.vue"),
    },
    {
      path: "/tools/metatalente/fallen-stellen",
      name: "metaTalentsTrapping",
      component: () => import("../views/public/tools/metaTalents/Trapping.vue"),
    },
    {
      path: "/tools/metatalente/wache-halten",
      name: "metaTalentsKeepWatch",
      component: () =>
        import("../views/public/tools/metaTalents/KeepWatch.vue"),
    },
    {
      path: "/datenbanken/:type/:name",
      name: "db",
      component: () => import("../views/public/Database.vue"),
    },
    {
      path: "/:pathMatch(.*)*",
      name: "notFound",
      component: () => import("../views/public/NotFound.vue"),
    },
  ],
});

export default router;
