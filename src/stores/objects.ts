import { defineStore } from "pinia";
import type {
  Plant,
  Animal,
  Equipment,
  AlchimicProduct,
  PlantFilterType,
  PlantPart,
} from "@/schemas";
import {
  alchimicProducts,
  animals,
  equipment,
  getActivePlants,
  plantParts,
} from "@/data";

interface objectsStore {
  alchimicProducts: Record<string, AlchimicProduct>;
  animals: Record<string, Animal>;
  equipment: Record<string, Equipment>;
  plants: Record<string, Plant>;
  plantParts: Record<string, PlantPart>;
  ready: boolean;
}

export const getObjectsStore = defineStore({
  id: "objectsStore",
  state: (): objectsStore => ({
    alchimicProducts: alchimicProducts,
    animals: animals,
    equipment: equipment,
    plants: getActivePlants(),
    plantParts: plantParts,
    ready: false,
  }),
  getters: {
    alchimicProductsList(): AlchimicProduct[] {
      const list: AlchimicProduct[] = [];
      for (const alchimicProductsKey in this.alchimicProducts) {
        list.push(this.alchimicProducts[alchimicProductsKey]);
      }
      return list;
    },
    animalsList(): Animal[] {
      const list: Animal[] = [];
      for (const animalsKey in this.animals) {
        list.push(this.animals[animalsKey]);
      }
      return list;
    },
    equipmentList(): Equipment[] {
      const list: Equipment[] = [];
      for (const equipmentKey in this.equipment) {
        list.push(this.equipment[equipmentKey]);
      }
      return list;
    },
    plantsList(): Plant[] {
      const list: Plant[] = [];
      for (const plantsKey in this.plants) {
        list.push(this.plants[plantsKey]);
      }
      return list;
    },
    filteredPlants: (state) => {
      return (plantFilter: PlantFilterType): Record<number, Plant[]> => {
        const filteredPlants: Record<number, Plant[]> = {};
        const plantsList: Plant[] = [];
        for (const plantsKey in state.plants) {
          plantsList.push(state.plants[plantsKey]);
        }
        plantsList.forEach((plant) => {
          //
          // Type
          //
          let typeFound = false;
          plantFilter.types.forEach((type) => {
            if (plant.types.includes(type)) typeFound = true;
          });
          if (!typeFound && plantFilter.types.length > 0) return;
          //
          // aventuric area
          //
          if (plantFilter.aventuricAreas.length > 0) {
            let found = false;
            plant.aventuricAreas.forEach((area) => {
              if (plantFilter.aventuricAreas.includes(area)) found = true;
            });
            if (!found) return;
          }
          //
          // spreads
          //
          const areaModifiers: number[] = [];
          if (plantFilter.spreads.length > 0) {
            let found = false;
            plant.spread(plantFilter.harvestTime).forEach((spread) => {
              plantFilter.spreads.forEach((filterSpread) => {
                if (spread.str() === filterSpread.str()) {
                  found = true;
                  areaModifiers.push(spread.rarity.value);
                }
              });
            });
            if (!found) return;
          } else {
            plant.spread(plantFilter.harvestTime).forEach((spread) => {
              areaModifiers.push(spread.rarity.value);
            });
          }
          //
          // identification min
          //
          if (
            plantFilter.identificationModifierMin &&
            plantFilter.identificationModifierMin.value >
              plant.identificationModifier(
                plantFilter.harvestTime,
                plantFilter.harvestDaytime,
                plantFilter.harvestMoonPhase
              ).value
          )
            return;
          if (
            plantFilter.identificationModifierMax &&
            plantFilter.identificationModifierMax.value <
              plant.identificationModifier(
                plantFilter.harvestTime,
                plantFilter.harvestDaytime,
                plantFilter.harvestMoonPhase
              ).value
          )
            return;
          //
          // harvest time
          //
          if (!plant.harvestTime.includes(plantFilter.harvestTime)) return;

          let areaModifier = 0;
          switch (plantFilter.costCalculation) {
            case "avg":
              areaModifiers.forEach((num) => {
                areaModifier += num;
              });
              areaModifier = areaModifier / areaModifiers.length;
              break;

            case "min":
              areaModifier = Math.max(...areaModifiers);
              break;

            case "max":
              areaModifier = Math.min(...areaModifiers);
              break;
          }

          const identificationModifier = plant.identificationModifier(
            plantFilter.harvestTime,
            plantFilter.harvestDaytime,
            plantFilter.harvestMoonPhase,
            plantFilter.spreads,
            plantFilter.magicModifierZFPStar
          ).value;

          let cost = Math.floor(
            areaModifier + identificationModifier + plantFilter.masterModifier
          );
          if (cost < 0) cost = Math.abs(cost);
          else cost = 1;

          if (!Object.keys(filteredPlants).includes(String(cost)))
            filteredPlants[cost] = [];
          filteredPlants[cost].push(plant);
        });
        return filteredPlants;
      };
    },
  },
});
