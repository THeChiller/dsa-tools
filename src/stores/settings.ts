import { defineStore } from "pinia";
import { darkTheme, lightTheme } from "naive-ui";

interface settingsStore {
  theme: typeof darkTheme | typeof lightTheme;
}

export const getSettingsStore = defineStore({
  id: "settings",
  state: (): settingsStore => ({
    theme: darkTheme,
  }),
});
