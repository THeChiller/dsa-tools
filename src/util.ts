import { useBreakpoint, useMemo } from "vooks";
import type { ComputedRef } from "vue";
import type { FilterOption } from "naive-ui/lib/data-table/src/interface";
import type { SelectOption } from "naive-ui";

export const version = "1.0.0";

// taken from https://github.com/TuSimple/naive-ui/blob/main/demo/utils/composables.js
// xs < 640 <= s < 1024 <= m < 1280 <= l < 1536 <= xl < 1920 <= xxl
export function isMobile(): ComputedRef {
  const breakpointRef = useBreakpoint();
  return useMemo(() => {
    return breakpointRef.value === "xs";
  });
}

export function isTablet(): ComputedRef {
  const breakpointRef = useBreakpoint();
  return useMemo(() => {
    return breakpointRef.value === "s";
  });
}

export function isLargeDesktop(): ComputedRef {
  const breakpointRef = useBreakpoint();
  return useMemo(() => {
    return (
      breakpointRef.value !== "xs" &&
      breakpointRef.value !== "s" &&
      breakpointRef.value !== "m" &&
      breakpointRef.value !== "l" &&
      breakpointRef.value !== "xl"
    );
  });
}

export function getNDataTableFilterOptionFromEnum(
  input: Record<string, string>
): FilterOption[] {
  let array: FilterOption[] = [];
  Object.keys(input).forEach((key) => {
    array.push({
      label: input[key],
      value: input[key],
    });
  });
  array = array.sort((one, two) => (one.label > two.label ? 1 : -1));
  return array;
}
export function getNSelectOptionsFromEnum(
  input: Record<string, string>
): SelectOption[] {
  const array: SelectOption[] = [];
  Object.keys(input).forEach((key) => {
    array.push({
      label: input[key],
      value: input[key],
    });
  });
  return array;
}

export function getEnumElement(
  key: string,
  input: Record<string, string>
): string {
  return input[key];
}

export function getEnumKeyByValue(
  value: string,
  input: Record<string, string>
): string {
  let keyOfValue = "";
  Object.keys(input).forEach((key) => {
    if (input[key] === value) keyOfValue = key;
  });
  return keyOfValue;
}

export function getD20Roll(): number {
  return Math.floor(Math.random() * 20) + 1;
}
export function getD6Roll(): number {
  return Math.floor(Math.random() * 6) + 1;
}

export function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
