import {
  AlchimicProduct,
  AlchimicProductType,
  AlchimicRecipeIngredient,
  Amount,
  Aquisition,
  Book,
  DiceType,
  Duration,
  Modificator,
  Price,
  Reference,
  TimeFrame,
  Unit,
} from "@/schemas";

export const alchimicProducts: Record<string, AlchimicProduct> = {
  "eingelegte Alraunenwurzel": new AlchimicProduct(
    "eingelegte Alraunenwurzel",
    [AlchimicProductType.crop],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [new AlchimicRecipeIngredient(new Amount(1), "Alraunen Pflanze")],
    "",
    "keins",
    {
      brewing: undefined,
      analysis: undefined,
    },
    "Verlängert die Haltbarkeit der Alraune. Nach etwa 24 Stunden sind die Bitterstoffe zersetzt, so dass die Alraune keine Giftwirkung mehr besitzt.",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 8, 1, DiceType.d6),
    new Price(undefined, 8),
    "",
    [new Reference(Book.zba3, [227])],
    ["Alraunen-Sud", "Schmerzwein", "Lotostrunk"]
  ),
  "Alraunen-Sud": new AlchimicProduct(
    "Alraunen-Sud",
    [AlchimicProductType.ingredient],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [new AlchimicRecipeIngredient(new Amount(1), "eingelegte Alraunenwurzel")],
    "",
    "keins",
    {
      brewing: Modificator["0"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 8, 1, DiceType.d6),
    new Price(undefined, 8),
    "",
    [new Reference(Book.zba3, [227])],
    []
  ),
  Argansud: new AlchimicProduct(
    "Argansud",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [new AlchimicRecipeIngredient(new Amount(1), "Arganstrauch Wurzel")],
    "",
    "keins",
    {
      brewing: Modificator["-5"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 8, 1, DiceType.d6),
    new Price(undefined, 8),
    "",
    [new Reference(Book.zba3, [228])],
    []
  ),
  Atanax: new AlchimicProduct(
    "Atanax",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.stone),
        "Atan-Kiefer-Rinde"
      ),
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.stone),
        "Salz"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-4"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 9, 1, DiceType.d6),
    new Price(undefined, 8),
    "",
    [new Reference(Book.zba3, [229])],
    []
  ),
  Atmonbrei: new AlchimicProduct(
    "Atmonbrei",
    [AlchimicProductType.buff],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [new AlchimicRecipeIngredient(new Amount(1), "Atmon-Büschel")],
    "",
    "keins",
    {
      brewing: Modificator["-3"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 9, 1, DiceType.d6),
    new Price(18),
    "",
    [new Reference(Book.zba3, [229])],
    []
  ),
  Xordai: new AlchimicProduct(
    "Xordai",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(70, undefined, undefined, Unit.stone),
        "Axorda-Baum"
      ),
      new AlchimicRecipeIngredient(
        new Amount(910, undefined, undefined, Unit.stone),
        "Tarnele Pflanze"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-8"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 4, 1, DiceType.d6),
    new Price(14),
    "",
    [new Reference(Book.zba3, [229]), new Reference(Book.ga, [209])],
    []
  ),
  "Getrockneter Belmart": new AlchimicProduct(
    "Getrockneter Belmart",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [new AlchimicRecipeIngredient(new Amount(), "Belmart-Blätter")],
    "",
    "keins",
    {
      brewing: Modificator["-3"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 2, 1, DiceType.d3),
    new Price(4),
    "",
    [new Reference(Book.zba3, [230])],
    []
  ),
  "Calorg-Essenz": new AlchimicProduct(
    "Calorg-Essenz",
    [AlchimicProductType.buff],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(new Amount(5), "Carlog-Blüten"),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, undefined, Unit.flux),
        "Alkohol"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-2"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 2, 1, DiceType.d3),
    new Price(4),
    "",
    [new Reference(Book.zba3, [232])],
    []
  ),
  Cheriacha: new AlchimicProduct(
    "Cheriacha",
    [AlchimicProductType.drug],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [new AlchimicRecipeIngredient(new Amount(), "Cheria-Kaktusfleisch")],
    "",
    "keins",
    {
      brewing: Modificator["0"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 12, 2, DiceType.d6),
    new Price(undefined, 5),
    "",
    [new Reference(Book.zba3, [233])],
    [],
    new Duration(TimeFrame.kr, 30),
    1
  ),
  Brandsalbe: new AlchimicProduct(
    "Brandsalbe",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(new Amount(3), "Chonchinis-Milch"),
      new AlchimicRecipeIngredient(new Amount(1), "Wirselkraut-Büschel"),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-2"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 18, 2, DiceType.d6),
    new Price(7),
    "",
    [new Reference(Book.zba3, [233])],
    []
  ),
  Praiosmilch: new AlchimicProduct(
    "Praiosmilch",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(new Amount(2), "Chonchinis-Milch"),
      new AlchimicRecipeIngredient(new Amount(1), "Praiosblumenöl"),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-5"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 3, 1, DiceType.d3),
    new Price(3),
    "",
    [new Reference(Book.zba3, [233])],
    []
  ),
  Kelmon: new AlchimicProduct(
    "Kelmon",
    [AlchimicProductType.poison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(new Amount(4), "Disdychonda-Blätter"),
      new AlchimicRecipeIngredient(new Amount(1), "Braunöl"),
      new AlchimicRecipeIngredient(new Amount(1), "Schwefliger Orazal"),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-8"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 9, 1, DiceType.d6),
    new Price(25),
    "",
    [new Reference(Book.zba3, [234])],
    [],
    new Duration(TimeFrame.kr, 5),
    5
  ),
  "Eingelegter Donf": new AlchimicProduct(
    "Eingelegter Donf",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(new Amount(1), "Donf-Stängel"),
      new AlchimicRecipeIngredient(new Amount(1), "Alkohol"),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-2"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 9, 1, DiceType.d6),
    new Price(4),
    "",
    [new Reference(Book.zba3, [234])],
    []
  ),
  "Dornrosen Öl": new AlchimicProduct(
    "Dornrosen Öl",
    [AlchimicProductType.cosmetics],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(25, undefined, undefined, Unit.stone),
        "Dornrosen Blüte"
      ),
      new AlchimicRecipeIngredient(
        new Amount(25, undefined, undefined, Unit.stone),
        "Dornrosen Ranke"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-7"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.years),
    new Price(undefined, 5),
    "",
    [new Reference(Book.zba3, [234])],
    []
  ),
  Egelschreckpaste: new AlchimicProduct(
    "Egelschreckpaste",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(5, undefined, undefined, Unit.count),
        "Egelschreck-Blätter"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Kalk"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Fett"
      ),
    ],
    "Paste anrühren",
    "keins",
    {
      brewing: Modificator["-4"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 6, 1, DiceType.d6),
    new Price(undefined, 3),
    "",
    [new Reference(Book.zba3, [235])],
    []
  ),
  "Krötenschemel-Gift (Waffengift)": new AlchimicProduct(
    "Krötenschemel-Gift",
    [AlchimicProductType.weaponPoison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.ounce),
        "Nussöl"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.ounce),
        "Schwefliger Orazal"
      ),
      new AlchimicRecipeIngredient(
        new Amount(4, undefined, undefined, Unit.count),
        "Eitriger Krötenschemel Pilzhaut"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-8"],
      analysis: Modificator["-4"],
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 1, 1, DiceType.d3),
    new Price(10),
    "",
    [new Reference(Book.zba3, [236])],
    [],
    new Duration(TimeFrame.hours),
    4
  ),
  "Krötenschemel-Gift (Einnahme)": new AlchimicProduct(
    "Krötenschemel-Gift (Einnahme)",
    [AlchimicProductType.poison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.schank),
        "Alkohol"
      ),
      new AlchimicRecipeIngredient(
        new Amount(4, undefined, undefined, Unit.count),
        "Eitriger Krötenschemel Pilzhaut"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-8"],
      analysis: Modificator["-4"],
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 1, 1, DiceType.d3),
    new Price(12),
    "",
    [new Reference(Book.zba3, [235])],
    [],
    new Duration(TimeFrame.hours),
    4
  ),
  "Feuerschlick-Pulver": new AlchimicProduct(
    "Feuerschlick-Pulver",
    [AlchimicProductType.buff],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(10, undefined, undefined, Unit.ounce),
        "Feuerschlick Algen"
      ),
      new AlchimicRecipeIngredient(
        new Amount(10, undefined, undefined, Unit.schank),
        "Meerwasser"
      ),
      new AlchimicRecipeIngredient(
        new Amount(10, undefined, undefined, Unit.scrupel),
        "Schwefliger Orazal"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.flux),
        "Alkohol"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.carat),
        "Ambra"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-6"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 6, 1, DiceType.d6),
    new Price(undefined, 1),
    "",
    [new Reference(Book.zba3, [237])],
    []
  ),
  "Feuerschlick-Essenz": new AlchimicProduct(
    "Feuerschlick-Essenz",
    [AlchimicProductType.buff],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(10, undefined, undefined, Unit.ounce),
        "Feuerschlick Algen"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.flux),
        "Alkohol"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.flux),
        "Lulanientrunk"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-8"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 6, 1, DiceType.d6),
    new Price(1),
    "",
    [new Reference(Book.zba3, [237])],
    []
  ),
  "Finage Sud": new AlchimicProduct(
    "Finage Sud",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(7, undefined, undefined, Unit.count),
        "Finage Triebe"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["0"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 3, 1, DiceType.d3),
    new Price(undefined, 6),
    "",
    [new Reference(Book.zba3, [238])],
    []
  ),
  Finagebast: new AlchimicProduct(
    "Finagebast",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(7, undefined, undefined, Unit.count),
        "Finage Bast"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-3"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 4, 1, DiceType.d3),
    new Price(8),
    "",
    [new Reference(Book.zba3, [238])],
    []
  ),
  Gulmondtee: new AlchimicProduct(
    "Gulmondtee",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.count),
        "Gulmond Blatt"
      ),
    ],
    "",
    "keins",
    {
      brewing: undefined,
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.hours, 24, 1, DiceType.d6),
    new Price(6),
    "",
    [new Reference(Book.zba3, [239])],
    []
  ),
  "Starker Gulmond": new AlchimicProduct(
    "Starker Gulmond",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(5, undefined, undefined, Unit.count),
        "Gulmond Blatt"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["3"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.hours, 18, 2, DiceType.d6),
    new Price(4),
    "",
    [new Reference(Book.zba3, [239])],
    []
  ),
  "Doppelter Gulmond": new AlchimicProduct(
    "Doppelter Gulmond",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(6, undefined, undefined, Unit.count),
        "Gulmond Blatt"
      ),
      new AlchimicRecipeIngredient(
        new Amount(undefined, undefined, undefined, Unit.count),
        "Minze"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["1"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.hours, 18, 1, DiceType.d6),
    new Price(4),
    "",
    [new Reference(Book.zba3, [239])],
    []
  ),
  "Hirad-Antidot": new AlchimicProduct(
    "Hirad-Antidot",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(6, undefined, undefined, Unit.count),
        "Hiradwurz Wurzel"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-5"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 9, 1, DiceType.d6),
    new Price(12),
    "",
    [new Reference(Book.zba3, [239])],
    []
  ),
  Hollbeertee: new AlchimicProduct(
    "Hollbeertee",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(7, undefined, undefined, Unit.count),
        "Hollbeere Blatt"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-5"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 3, 1, DiceType.d6),
    new Price(undefined, 3),
    "",
    [new Reference(Book.zba3, [240])],
    []
  ),
  Wurara: new AlchimicProduct(
    "Wurara",
    [AlchimicProductType.weaponPoison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(0.5, undefined, undefined, Unit.stone),
        "Höllenkraut Ranke"
      ),
      new AlchimicRecipeIngredient(
        new Amount(0.5, undefined, undefined, Unit.ounce),
        "Kalk"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-4"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 1, 1, DiceType.d6),
    new Price(10),
    "",
    [new Reference(Book.zba3, [240])],
    [],
    new Duration(TimeFrame.sr, 1),
    4
  ),
  Horuschenöl: new AlchimicProduct(
    "Horuschenöl",
    [AlchimicProductType.buff],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(7, undefined, undefined, Unit.stone),
        "Horusche Kern"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.schank),
        "Vitriol"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.schank),
        "Alkohol"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-6"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 6, 1, DiceType.d6),
    new Price(30),
    "",
    [new Reference(Book.zba3, [240])],
    [],
    new Duration(TimeFrame.kr, 10),
    1
  ),
  Ilmenblatt: new AlchimicProduct(
    "Ilmenblatt",
    [AlchimicProductType.drug],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(7, undefined, undefined, Unit.stone),
        "Ilmenblatt Blatt und Blüte"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["0"],
      analysis: Modificator["-4"],
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 24, 2, DiceType.d6),
    new Price(6),
    "",
    [new Reference(Book.zba3, [241])],
    []
  ),
  "Fiebersaft (wässriger Sud)": new AlchimicProduct(
    "Fiebersaft (wässriger Sud)",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(4, undefined, undefined, Unit.count),
        "Joruga Wurzel"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["0"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.weeks, 3, 1, DiceType.d3),
    new Price(8),
    "",
    [new Reference(Book.zba3, [241])],
    []
  ),
  "Fiebersaft (alkoholischer Sud)": new AlchimicProduct(
    "Fiebersaft (alkoholischer Sud)",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(4, undefined, undefined, Unit.count),
        "Joruga Wurzel"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Alkohol"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-2"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 2, 1, DiceType.d3),
    new Price(12),
    "",
    [new Reference(Book.zba3, [241])],
    []
  ),
  Astraltrunk: new AlchimicProduct(
    "Astraltrunk",
    [AlchimicProductType.buff],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Kairan Halm"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Mibelkolben und Stängel"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Alkohol"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-8"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 5, 1, DiceType.d3),
    new Price(15),
    "moegliche linderung der sucht durch element erz/fels (wdz 380) -> sdr 121, oder sdr 119 einhorn-horn",
    [new Reference(Book.zba3, [244])],
    []
  ),
  "Kajubo in Öl": new AlchimicProduct(
    "Kajubo in Öl",
    [AlchimicProductType.buff],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(5, undefined, undefined, Unit.count),
        "Kajubo Knospe"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Nussöl"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-4"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.weeks, 20, 1, DiceType.d6),
    new Price(10),
    "",
    [new Reference(Book.zba3, [244])],
    []
  ),
  Klippenzahnsaft: new AlchimicProduct(
    "Klippenzahnsaft",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(10, undefined, undefined, Unit.count),
        "Klippenzahn Stängel"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-4"],
      analysis: Modificator["-7"],
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 1),
    new Price(1),
    "Maximal bis zum nächten Vollmond haltbar",
    [new Reference(Book.zba3, [245])],
    []
  ),
  "Klippenzahnsaft (aufbereitet)": new AlchimicProduct(
    "Klippenzahnsaft (aufbereitet)",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(10, undefined, undefined, Unit.count),
        "Klippenzahn Stängel"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-4"],
      analysis: Modificator["-7"],
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 6),
    new Price(4),
    "maximal bis zur nächten Tagundnachtgleiche haltbar (30. Efferd, 30. Phex)",
    [new Reference(Book.zba3, [245])],
    []
  ),
  "Purpurner Lotosstaub": new AlchimicProduct(
    "Purpurner Lotosstaub",
    [AlchimicProductType.poison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(5, undefined, undefined, Unit.count),
        "Purpurner Lotos Blüte"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-4"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 4, 1, DiceType.d6),
    new Price(5),
    "",
    [new Reference(Book.zba3, [246])],
    [],
    undefined,
    8
  ),
  "Schwarzer Lotosstaub": new AlchimicProduct(
    "Schwarzer Lotosstaub",
    [AlchimicProductType.poison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(5, undefined, undefined, Unit.count),
        "Schwarzer Lotos Blüte"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.ounce),
        "Schwefelquell"
      ),
    ],
    "Probe auch Pflanzenkunde -10",
    "keins",
    {
      brewing: Modificator["-15"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.days, 15, 1, DiceType.d6),
    new Price(100),
    "",
    [new Reference(Book.zba3, [246])],
    [],
    new Duration(TimeFrame.kr, 5),
    10
  ),
  "Marbos Odem": new AlchimicProduct(
    "Marbos Odem",
    [AlchimicProductType.poison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(5, undefined, undefined, Unit.count),
        "Weißer Lotos Blüte"
      ),
      new AlchimicRecipeIngredient(
        new Amount(2, undefined, undefined, Unit.flux),
        "Salzlake"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-5"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.days, 14, 1, DiceType.d6),
    new Price(20),
    "",
    [new Reference(Book.zba3, [247])],
    []
  ),
  Lotostrunk: new AlchimicProduct(
    "Lotostrunk",
    [AlchimicProductType.buff],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(5, undefined, undefined, Unit.count),
        "Weißgelber Lotos Blüte"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Alkohol"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "eingelegte Alraunenwurzel"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-15"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.weeks, 6, 1, DiceType.d3),
    new Price(25),
    "",
    [new Reference(Book.zba3, [247])],
    []
  ),
  Lulanientrunk: new AlchimicProduct(
    "Lulanientrunk",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Lulanie Blüte"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-3"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 5, 1, DiceType.d3),
    new Price(7),
    "",
    [new Reference(Book.zba3, [248])],
    ["Feuerschlick-Essenz"]
  ),
  Liebesküchlein: new AlchimicProduct(
    "Liebesküchlein",
    [AlchimicProductType.buff],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Lulanie Blüte"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.flux),
        "Katzenmilch"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-4"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 1, undefined, undefined),
    new Price(2),
    "",
    [new Reference(Book.zba3, [248])],
    []
  ),
  "Malomis-Wasser": new AlchimicProduct(
    "Malomis-Wasser",
    [AlchimicProductType.drug],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.count),
        "Malomis Blüte"
      ),
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.count),
        "Carlog-Blüten"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.flux),
        "Alkohol"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-7"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.weeks, 6, 1, DiceType.d3),
    new Price(85),
    "",
    [new Reference(Book.zba3, [249])],
    []
  ),
  "1001 Rausch": new AlchimicProduct(
    "1001 Rausch",
    [AlchimicProductType.cosmetics],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Malomis Blüte"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.flux),
        "Rosenwasser"
      ),
      new AlchimicRecipeIngredient(
        new Amount(2, undefined, undefined, Unit.flux),
        "Alkohol"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-15"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 6, 1, DiceType.d6),
    new Price(60),
    "",
    [new Reference(Book.zba3, [249])],
    []
  ),
  "Menchal Blüte": new AlchimicProduct(
    "Menchal Blüte",
    [AlchimicProductType.spice],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Menchal Blüte"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["0"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 3, 1, DiceType.d3),
    new Price(7),
    "",
    [new Reference(Book.zba3, [249])],
    []
  ),
  "Merach-Extrakt": new AlchimicProduct(
    "Merach-Extrakt",
    [AlchimicProductType.poison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.count),
        "Merach Frucht"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.schank),
        "Alkohol"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Honig"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.ounce),
        "Schwefliger Orazal"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-4"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, undefined, 1, DiceType.d6),
    new Price(20),
    "",
    [new Reference(Book.zba3, [250])],
    [],
    new Duration(TimeFrame.sr, 1),
    8
  ),
  "Mibel-Absud": new AlchimicProduct(
    "Mibel-Absud",
    [AlchimicProductType.drug],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.count),
        "Mibelkolben und Stängel"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.flux),
        "Alkohol"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-7"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 4, 1, DiceType.d6),
    new Price(6),
    "",
    [new Reference(Book.zba3, [251])],
    []
  ),
  Stinkmirbel: new AlchimicProduct(
    "Stinkmirbel",
    [AlchimicProductType.repellent],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.count),
        "Mirbelwurzelknolle"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-1"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.weeks, 2, 1, DiceType.d3),
    new Price(undefined, 3),
    "",
    [new Reference(Book.zba3, [251])],
    []
  ),
  Kukris: new AlchimicProduct(
    "Kukris",
    [AlchimicProductType.weaponPoison, AlchimicProductType.poison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.count),
        "Mirhamer Seidenliane Knoten"
      ),
      new AlchimicRecipeIngredient(
        new Amount(2, undefined, undefined, Unit.ounce),
        "Schwefliger Orazal"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.schank),
        "Vitriol"
      ),
    ],
    "(Kochen hat eine Brauschwierigkeit von -9)",
    "keins",
    {
      brewing: Modificator["-6"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 2, 1, DiceType.d3),
    new Price(90),
    "",
    [new Reference(Book.zba3, [251])],
    [],
    new Duration(TimeFrame.kr, 15),
    12
  ),
  Schmerzwein: new AlchimicProduct(
    "Schmerzwein",
    [AlchimicProductType.painkiller],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.count),
        "Bleichmohn Samenkapsel"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined),
        "Alkohol"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "eingelegte Alraunenwurzel"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Weinstein"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-10"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.days, 6, 1, DiceType.d6),
    new Price(30),
    "",
    [new Reference(Book.zba3, [251])],
    []
  ),
  Goldleim: new AlchimicProduct(
    "Goldleim",
    [AlchimicProductType.weaponPoison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Naftanstaude Saft"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.ounce),
        "Vitriol"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Schiefer"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-6"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 6, 1, DiceType.d6),
    new Price(30),
    "",
    [new Reference(Book.zba3, [255])],
    [],
    new Duration(TimeFrame.kr, 0),
    5
  ),
  "Efferdhilf (Absud)": new AlchimicProduct(
    "Efferdhilf (Absud)",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Neckerkraut Blatt"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-3"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.hours, 24, 1, DiceType.d6),
    new Price(undefined, 1),
    "",
    [new Reference(Book.zba3, [256])],
    []
  ),
  "Efferdhilf (Pille)": new AlchimicProduct(
    "Efferdhilf (Pille)",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Neckerkraut Blatt"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-7"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.weeks, 9, 1, DiceType.d6),
    new Price(undefined, 1),
    "",
    [new Reference(Book.zba3, [256])],
    []
  ),
  Nothilf: new AlchimicProduct(
    "Nothilf",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(5, undefined, undefined, Unit.count),
        "Nothilf Blüte"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.flux),
        "Öl"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-2"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 6, 1, DiceType.d3),
    new Price(30),
    "",
    [new Reference(Book.zba3, [256])],
    []
  ),
  Brandhilf: new AlchimicProduct(
    "Brandhilf",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.count),
        "Nothilf Blatt"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Öl"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-2"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 6, 1, DiceType.d3),
    new Price(7),
    "",
    [new Reference(Book.zba3, [256])],
    []
  ),
  Olginsud: new AlchimicProduct(
    "Olginsud",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(5, undefined, undefined, Unit.count),
        "Olginwurz Moosballen"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Alkohol"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-4"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 6, 2, DiceType.d6),
    new Price(50),
    "",
    [new Reference(Book.zba3, [257])],
    []
  ),
  Orazalkleber: new AlchimicProduct(
    "Orazalkleber",
    [AlchimicProductType.glue],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Orazal verholzter Stängel"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-6"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 15, 2, DiceType.d6),
    new Price(5),
    "",
    [new Reference(Book.zba3, [257])],
    []
  ),
  "Schwefliger Orazal": new AlchimicProduct(
    "Schwefliger Orazal",
    [AlchimicProductType.preservative],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Orazal verholzter Stängel"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Schwefel"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Weinstein"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-8"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.years, 20),
    new Price(8),
    "",
    [new Reference(Book.zba3, [257])],
    []
  ),
  Katzenaugensalbe: new AlchimicProduct(
    "Katzenaugensalbe",
    [AlchimicProductType.drug],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Pestsporenpilz Pilzhaut"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Alkohol"
      ),
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.count),
        "Carlog-Blüten"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.ounce),
        "Salbengrundstoff"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-8"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 12, 1, DiceType.d6),
    new Price(40),
    "",
    [new Reference(Book.zba3, [258])],
    []
  ),
  "Kaltes Feuer": new AlchimicProduct(
    "Kaltes Feuer",
    [AlchimicProductType.illuminant],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(0.5, undefined, undefined, Unit.ounce),
        "Phosphorpilzgeflecht"
      ),
      new AlchimicRecipeIngredient(
        new Amount(0.25, undefined, undefined, Unit.ounce),
        "Schwefel"
      ),
      new AlchimicRecipeIngredient(
        new Amount(0.25, undefined, undefined, Unit.ounce),
        "Kalk"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-2"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 18, 2, DiceType.d6),
    new Price(undefined, 5),
    "",
    [new Reference(Book.zba3, [259])],
    []
  ),
  Quasselpulver: new AlchimicProduct(
    "Quasselpulver",
    [AlchimicProductType.poison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Quasselwurz Wurzel"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Basiliskum"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-5"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 24, 3, DiceType.d6),
    new Price(),
    "",
    [new Reference(Book.zba3, [259])],
    [],
    new Duration(TimeFrame.sr, 1),
    4
  ),
  "getrocknete Quinja-Beeren": new AlchimicProduct(
    "getrocknete Quinja-Beeren",
    [AlchimicProductType.buff],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.count),
        "Quinja Beere"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Schwefel"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-5"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 20, 1, DiceType.d20),
    new Price(4),
    "",
    [new Reference(Book.zba3, [260]), new Reference(Book.srd, [93])],
    ["Kraftelixier"]
  ),
  "Rahjalieb-Tee": new AlchimicProduct(
    "Rahjalieb-Tee",
    [AlchimicProductType.birthControl],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(12, undefined, undefined, Unit.count),
        "Rahjalieb Blatt"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-3"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 3, 1, DiceType.d3),
    new Price(undefined, 2),
    "",
    [new Reference(Book.zba3, [260])],
    []
  ),
  "Rauschgurke (maraskanisch)": new AlchimicProduct(
    "Rauschgurke (maraskanisch)",
    [AlchimicProductType.spice, AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Rauschgurke Gurke"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-15"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.years, 5),
    new Price(10),
    "",
    [new Reference(Book.zba3, [261])],
    []
  ),
  Pfeilblütentee: new AlchimicProduct(
    "Pfeilblütentee",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.count),
        "Rote Pfeilblüte Blüte"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-1"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 5, 1, DiceType.d3),
    new Price(undefined, 2),
    "",
    [new Reference(Book.zba3, [261])],
    []
  ),
  Pfeilblütenpunsch: new AlchimicProduct(
    "Pfeilblütenpunsch",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Schwefliger Orazal"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.flux),
        "Alkohol"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-4"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 5, 1, DiceType.d3),
    new Price(undefined, 2),
    "",
    [new Reference(Book.zba3, [261])],
    []
  ),
  "Roter Drachenodem": new AlchimicProduct(
    "Roter Drachenodem",
    [AlchimicProductType.smokingAgent],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(5, undefined, undefined, Unit.count),
        "Roter Drachenschlund Blatt"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-6"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.years, 9, 1, DiceType.d6),
    new Price(25),
    "",
    [new Reference(Book.zba3, [262])],
    []
  ),
  Sansarosud: new AlchimicProduct(
    "Sansarosud",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(0.5, undefined, undefined, Unit.stone),
        "Sansaro Algen"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Kalk"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-5"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 9, 1, DiceType.d6),
    new Price(6),
    "",
    [new Reference(Book.zba3, [262])],
    []
  ),
  Sansaropaste: new AlchimicProduct(
    "Sansaropaste",
    [AlchimicProductType.repellent],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(0.5, undefined, undefined, Unit.stone),
        "Sansaro Algen"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Kalk"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Tran"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-5"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.weeks, 7, 1, DiceType.d6),
    new Price(3),
    "",
    [new Reference(Book.zba3, [262])],
    []
  ),
  Satuarienstrunk: new AlchimicProduct(
    "Satuarienstrunk",
    [AlchimicProductType.buff],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(2, undefined, undefined, Unit.count),
        "Satuariensbusch Blatt"
      ),
      new AlchimicRecipeIngredient(
        new Amount(2, undefined, undefined, Unit.count),
        "Satuariensbusch Blüte"
      ),
      new AlchimicRecipeIngredient(
        new Amount(2, undefined, undefined, Unit.count),
        "Satuariensbusch Frucht"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-3"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 9, 1, DiceType.d6),
    new Price(undefined, undefined, 5),
    "",
    [new Reference(Book.zba3, [263])],
    []
  ),
  Magiepaste: new AlchimicProduct(
    "Magiepaste",
    [AlchimicProductType.utils],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Schlangenzünglein Saft"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Fett"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-10"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 5, 1, DiceType.d3),
    new Price(7),
    "",
    [new Reference(Book.zba3, [263])],
    []
  ),
  "Samthauch (Droge)": new AlchimicProduct(
    "Samthauch (Droge)",
    [AlchimicProductType.drug],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(2, undefined, undefined, Unit.count),
        "Schleichender Tod Blüte"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-3"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 5, 1, DiceType.d3),
    new Price(4),
    "Preis pro Staub einer Blüte",
    [new Reference(Book.zba3, [264])],
    [],
    new Duration(TimeFrame.sr, 1),
    2
  ),
  "Samthauch (Gift)": new AlchimicProduct(
    "Samthauch (Gift)",
    [AlchimicProductType.breathingPoison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(5, undefined, undefined, Unit.count),
        "Schleichender Tod Blüte"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-3"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 5, 1, DiceType.d3),
    new Price(4),
    "Preis pro Staub einer Blüte",
    [new Reference(Book.zba3, [264])],
    [],
    new Duration(TimeFrame.sr, 1),
    2
  ),
  "Knötergift (Waffengift)": new AlchimicProduct(
    "Knötergift (Waffengift)",
    [AlchimicProductType.weaponPoison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.count),
        "Schleimiger Sumpfknöterich Pilz"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Salzlake"
      ),
      new AlchimicRecipeIngredient(
        new Amount(5, undefined, undefined, Unit.ounce),
        "Knochenleim"
      ),
      new AlchimicRecipeIngredient(
        new Amount(2, undefined, undefined, Unit.ounce),
        "Salbengrundstoff"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-6"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.weeks, 4, 1, DiceType.d6),
    new Price(50),
    "",
    [new Reference(Book.zba3, [264])],
    [],
    new Duration(TimeFrame.kr, 0),
    8
  ),
  "Knötergift (Kontaktgift)": new AlchimicProduct(
    "Knötergift (Kontaktgift)",
    [AlchimicProductType.contactPoison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.count),
        "Schleimiger Sumpfknöterich Pilz"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Salzlake"
      ),
      new AlchimicRecipeIngredient(
        new Amount(5, undefined, undefined, Unit.ounce),
        "Knochenleim"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Farbstoff"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-6"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.weeks, 4, 1, DiceType.d6),
    new Price(50),
    "",
    [new Reference(Book.zba3, [264])],
    [],
    new Duration(TimeFrame.kr, 0),
    8
  ),
  Belkelinen: new AlchimicProduct(
    "Belkelinen",
    [AlchimicProductType.poison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Schwarzer Wein Traube"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["2"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 7, 1, DiceType.d6),
    new Price(2),
    "für 50 Beeren (2 Unzen)",
    [new Reference(Book.zba3, [266])],
    [],
    new Duration(TimeFrame.sr, 1),
    1
  ),
  "Schwarzer Wein": new AlchimicProduct(
    "Schwarzer Wein",
    [AlchimicProductType.drug],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Schwarzer Wein Traube"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-10"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.years, undefined, 7, DiceType.d20),
    new Price(7),
    "",
    [new Reference(Book.zba3, [266])],
    [],
    new Duration(TimeFrame.kr, 21),
    4
  ),
  "Shurinstrauchknollengift (Einnahmegift)": new AlchimicProduct(
    "Shurinstrauchknollengift (Einnahmegift)",
    [AlchimicProductType.poison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.count),
        "Shurinstrauch Knolle"
      ),
      new AlchimicRecipeIngredient(
        new Amount(9, undefined, undefined, Unit.count),
        "Shurinstrauch Blatt"
      ),
      new AlchimicRecipeIngredient(
        new Amount(0.5, undefined, undefined, Unit.ounce),
        "Tierfett"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.scrupel),
        "Knochenmehl"
      ),
    ],
    "Die Blätter ODER die Knollen verwenden.",
    "keins",
    {
      brewing: Modificator["-7"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 1, 1, DiceType.d3),
    new Price(70),
    "",
    [new Reference(Book.zba3, [267])],
    [],
    new Duration(TimeFrame.sr, 6),
    6
  ),
  "Shurinstrauchknollengift (Waffengift)": new AlchimicProduct(
    "Shurinstrauchknollengift (Waffengift)",
    [AlchimicProductType.weaponPoison],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(3, undefined, undefined, Unit.count),
        "Shurinstrauch Knolle"
      ),
      new AlchimicRecipeIngredient(
        new Amount(9, undefined, undefined, Unit.count),
        "Shurinstrauch Blatt"
      ),
      new AlchimicRecipeIngredient(
        new Amount(0.5, undefined, undefined, Unit.ounce),
        "Tierfett"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.scrupel),
        "Knochenmehl"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Öl"
      ),
    ],
    "Die Blätter ODER die Knollen verwenden.",
    "keins",
    {
      brewing: Modificator["-7"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 1, 1, DiceType.d3),
    new Price(70),
    "",
    [new Reference(Book.zba3, [267])],
    [],
    new Duration(TimeFrame.sr, 6),
    6
  ),
  Talaschinsalbe: new AlchimicProduct(
    "Talaschinsalbe",
    [AlchimicProductType.utils],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Talaschin Flechte"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Salbengrundstoff"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-2"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 9, 1, DiceType.d3),
    new Price(7),
    "",
    [new Reference(Book.zba3, [268])],
    []
  ),
  Tarnelensalbe: new AlchimicProduct(
    "Tarnelensalbe",
    [AlchimicProductType.heal],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Tarnele Pflanze"
      ),
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Salbengrundstoff"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["-6"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 1),
    new Price(undefined, 2),
    "Haltbarkeit: TaP* Monate",
    [new Reference(Book.zba3, [268])],
    []
  ),
  "Thonnys getrocknet": new AlchimicProduct(
    "Thonnys getrocknet",
    [AlchimicProductType.buff],
    new Aquisition(new Price(), new Price(), Modificator["7"]),
    [
      new AlchimicRecipeIngredient(
        new Amount(1, undefined, undefined, Unit.count),
        "Thonnys Blatt"
      ),
    ],
    "",
    "keins",
    {
      brewing: Modificator["0"],
      analysis: undefined,
    },
    "",
    Modificator["7"],
    "",
    new Duration(TimeFrame.months, 9, 1, DiceType.d6),
    new Price(4),
    "",
    [new Reference(Book.zba3, [269])],
    ["Zaubertrank", "Willenstrank", "Aufputschmittel"]
  ),
};
