import type { HarvestAmount } from "@/schemas";
import {
  Amount,
  AventuricArea,
  AventuricMonth,
  Book,
  Daytime,
  DiceType,
  GeographicArea,
  getAllSpreadsExcept,
  Modificator,
  Moonphase,
  Plant,
  plant_harvest_timeframe_all,
  plant_harvest_timeframe_autumn,
  plant_harvest_timeframe_spring,
  plant_harvest_timeframe_summer,
  PlantType,
  Rarity,
  Reference,
  Spread,
  Unit,
} from "@/schemas";
import { plantParts } from "@/data";

export function getActivePlants(): Record<string, Plant> {
  const activePlants: Record<string, Plant> = {};
  for (const plantsKey in plants) {
    if (plants[plantsKey].active) {
      activePlants[plantsKey] = plants[plantsKey];
    }
  }
  return activePlants;
}

const plants: Record<string, Plant> = {
  Alraune: new Plant(
    true,
    "Alraune",
    "Kontext",
    "Beschreibung",
    [PlantType.crop],
    [AventuricArea.allNorthKhomeDesert],
    [
      new Spread(GeographicArea.grasslandsHumid, Rarity.rare),
      new Spread(GeographicArea.forest, Rarity.rare),
    ],
    Modificator["-9"],
    [
      {
        amount: new Amount(1),
        plantPart: plantParts["Alraunen Pflanze"],
      },
    ],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
      AventuricMonth.firun,
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
    ],
    [
      new Reference(Book.zba3, [227]),
      new Reference(Book.srd, [81]),
      new Reference(Book.ga, [213]),
    ],
    undefined,
    undefined
  ),

  Alveranie: new Plant(
    true,
    "Alveranie",
    "Kontext",
    "Beschreibung",
    [PlantType.supernatural],
    [AventuricArea.all],
    getAllSpreadsExcept([], Rarity.extremeRare),
    Modificator["5"],
    [
      {
        amount: new Amount(12),
        plantPart: plantParts["Alveranien Blatt, in der Farbe des Monats"],
      },
    ],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
      AventuricMonth.firun,
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
    ],
    [new Reference(Book.zba3, [228])],
    undefined,
    undefined
  ),

  Arganstrauch: new Plant(
    true,
    "Arganstrauch",
    "Kontext",
    "Beschreibung",
    [PlantType.heal],
    [
      AventuricArea.mysob,
      AventuricArea.forestIslands,
      AventuricArea.forestAroundRainmountains,
    ],
    [
      new Spread(GeographicArea.swamp, Rarity.rare),
      new Spread(GeographicArea.rainForest, Rarity.sometimes),
      new Spread(GeographicArea.forest, Rarity.extremeRare),
    ],
    Modificator["-4"],
    [
      {
        amount: new Amount(1),
        plantPart: plantParts["Arganstrauch Wurzel"],
      },
    ],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
      AventuricMonth.firun,
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
    ],
    [new Reference(Book.zba3, [228])],
    undefined,
    undefined
  ),

  "Atan-Kiefer": new Plant(
    true,
    "Atan-Kiefer",
    "Kontext",
    "Beschreibung",
    [PlantType.heal],
    [AventuricArea.ehernsSword],
    [new Spread(GeographicArea.mountains, Rarity.rare)],
    Modificator["-6"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d20, Unit.stone),
        plantPart: plantParts["Atan-Kiefer-Rinde"],
      },
    ],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
      AventuricMonth.firun,
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
    ],
    [new Reference(Book.zba3, [228])],
    undefined,
    undefined
  ),

  Atmon: new Plant(
    true,
    "Atmon",
    "Kontext",
    "Beschreibung",
    [PlantType.crop],
    [AventuricArea.khomGoricDesert],
    [
      new Spread(GeographicArea.highland, Rarity.rare),
      new Spread(GeographicArea.steppe, Rarity.sometimes),
      new Spread(GeographicArea.riverMeadow, Rarity.extremeRare),
      new Spread(GeographicArea.swamp, Rarity.extremeRare),
    ],
    Modificator["-5"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d6),
        plantPart: plantParts["Atmon-Büschel"],
      },
    ],
    [AventuricMonth.peraine],
    [new Reference(Book.zba3, [229])],
    undefined,
    undefined
  ),

  Axorda: new Plant(
    true,
    "Axorda",
    "Kontext",
    "Beschreibung",
    [PlantType.crop],
    [AventuricArea.maraskan],
    [
      new Spread(GeographicArea.mountains, Rarity.extremeRare),
      new Spread(GeographicArea.rainForest, Rarity.extremeRare),
    ],
    Modificator["-4"],
    [
      {
        amount: new Amount(1),
        plantPart: plantParts["Axorda-Baum"],
      },
    ],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
      AventuricMonth.firun,
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
    ],
    [new Reference(Book.zba3, [229])],
    undefined,
    undefined
  ),

  Basilamine: new Plant(
    true,
    "Basilamine",
    "Kontext",
    "Beschreibung",
    [PlantType.dangerous],
    [AventuricArea.orcLand],
    [
      new Spread(GeographicArea.forest, Rarity.sometimes),
      new Spread(GeographicArea.forestEdge, Rarity.rare),
    ],
    Modificator["-15"],
    [
      {
        amount: new Amount(10, 1, DiceType.d20),
        plantPart: plantParts["Basilamine - Schoten"],
      },
    ],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
      AventuricMonth.firun,
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
    ],
    [new Reference(Book.zba3, [230])],
    (month?: AventuricMonth): Modificator => {
      if (month === AventuricMonth.ingerimm) return Modificator["-5"];
      return Modificator["-15"];
    },
    undefined
  ),

  Belmart: new Plant(
    true,
    "Belmart",
    "Kontext",
    "Beschreibung",
    [PlantType.heal],
    [AventuricArea.allNorthGrangorElburum],
    [
      new Spread(GeographicArea.forest, Rarity.sometimes),
      new Spread(GeographicArea.forestEdge, Rarity.rare),
    ],
    Modificator["-6"],
    [
      {
        amount: new Amount(undefined, 4, DiceType.d20),
        plantPart: plantParts["Belmart-Blätter"],
      },
    ],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
    ],
    [new Reference(Book.zba3, [230])],
    undefined,
    undefined
  ),
  Blutblatt: new Plant(
    true,
    "Blutblatt",
    "Kontext",
    "Beschreibung",
    [PlantType.crop],
    [AventuricArea.all],
    [new Spread(GeographicArea.astralSource, Rarity.rare)],
    Modificator["-4"],
    [
      {
        amount: new Amount(
          2,
          1,
          DiceType.d20,
          Unit.count,
          "pro 10 AsP der Quelle"
        ),
        plantPart: plantParts["Blutblatt-Zweige"],
      },
    ],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
      AventuricMonth.firun,
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
    ],
    [new Reference(Book.zba3, [230])],
    undefined,
    undefined
  ),

  Boronie: new Plant(
    true,
    "Boronie",
    "Kontext",
    "Beschreibung",
    [PlantType.supernatural],
    [AventuricArea.southAventurien],
    [
      new Spread(GeographicArea.grassland, Rarity.extremeRare),
      new Spread(GeographicArea.rainForest, Rarity.rare),
    ],
    Modificator["2"],
    [
      {
        amount: new Amount(5),
        plantPart: plantParts["Boronie-Blüten"],
      },
    ],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
      AventuricMonth.firun,
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
    ],
    [new Reference(Book.zba3, [231])],
    undefined,
    undefined
  ),

  Boronsschlinge: new Plant(
    true,
    "Boronsschlinge",
    "Kontext",
    "Beschreibung",
    [PlantType.dangerous],
    [AventuricArea.southAventurien, AventuricArea.middleAventurien],
    [
      new Spread(GeographicArea.rainForest, Rarity.extremeRare),
      new Spread(GeographicArea.forest, Rarity.extremeRare),
    ],
    Modificator["-15"],
    [],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
      AventuricMonth.firun,
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
    ],
    [new Reference(Book.zba3, [231])],
    undefined,
    undefined
  ),

  Carlog: new Plant(
    true,
    "Carlog",
    "Kontext",
    "Beschreibung",
    [PlantType.crop],
    [AventuricArea.westAventurien],
    [
      new Spread(GeographicArea.riverfronts, Rarity.rare),
      new Spread(GeographicArea.riverMeadow, Rarity.extremeRare),
      new Spread(GeographicArea.swamp, Rarity.sometimes),
    ],
    Modificator["-5"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d6),
        plantPart: plantParts["Carlog-Blüten"],
      },
    ],
    [AventuricMonth.efferd, AventuricMonth.peraine],
    [new Reference(Book.zba3, [232])],
    undefined,
    undefined
  ),

  "Cheria-Kaktus": new Plant(
    true,
    "Cheria-Kaktus",
    "Kontext",
    "Beschreibung",
    [PlantType.poison],
    [AventuricArea.southernKhom, AventuricArea.shadif, AventuricArea.szintotal],
    [new Spread(GeographicArea.desert, Rarity.rare)],
    Modificator["-4"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d6, Unit.halfStone),
        plantPart: plantParts["Cheria-Kaktusfleisch"],
      },
      {
        amount: new Amount(
          8,
          3,
          DiceType.d6,
          undefined,
          "pro Stein Kaktusfleisch"
        ),
        plantPart: plantParts["Cheria-Stacheln"],
      },
    ],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
      AventuricMonth.firun,
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
    ],
    [new Reference(Book.zba3, [232])],
    undefined,
    undefined
  ),

  Chonchinis: new Plant(
    true,
    "Chonchinis",
    "Kontext",
    "Beschreibung",
    [PlantType.heal],
    [AventuricArea.middleAventurien],
    [
      new Spread(GeographicArea.highland, Rarity.extremeRare),
      new Spread(GeographicArea.steppe, Rarity.sometimes),
      new Spread(GeographicArea.forestEdge, Rarity.rare),
    ],
    Modificator["-6"],
    [
      {
        amount: new Amount(0, 1, DiceType.d20),
        plantPart: plantParts["Chonchinis-Blätter"],
      },
      {
        amount: new Amount(1),
        plantPart: plantParts["Chonchinis-Milch"],
      },
    ],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
    ],
    [new Reference(Book.zba3, [233])],
    undefined,
    undefined
  ),

  Disdychonda: new Plant(
    true,
    "Disdychonda",
    "Kontext",
    "Beschreibung",
    [PlantType.dangerous],
    [
      AventuricArea.southEteren,
      AventuricArea.lizardSwamps,
      AventuricArea.southMaraskan,
    ],
    [
      new Spread(GeographicArea.rainForest, Rarity.extremeRare),
      new Spread(GeographicArea.forest, Rarity.extremeRare),
    ],
    Modificator["-5"],
    [{ amount: new Amount(4), plantPart: plantParts["Disdychonda-Blätter"] }],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
      AventuricMonth.firun,
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
    ],
    [new Reference(Book.zba3, [234])],
    undefined,
    undefined
  ),

  Raubnesseln: new Plant(
    true,
    "Raubnesseln",
    "Kontext",
    "Beschreibung",
    [PlantType.dangerous],
    [
      AventuricArea.southEteren,
      AventuricArea.lizardSwamps,
      AventuricArea.southMaraskan,
    ],
    [
      new Spread(GeographicArea.rainForest, Rarity.extremeRare),
      new Spread(GeographicArea.forest, Rarity.extremeRare),
    ],
    Modificator["-5"],
    [],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
      AventuricMonth.firun,
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
    ],
    [new Reference(Book.zba3, [234])],
    undefined,
    undefined
  ),

  Donf: new Plant(
    true,
    "Donf",
    "Kontext",
    "Beschreibung",
    [PlantType.heal],
    [AventuricArea.southAventurien, AventuricArea.middleAventurien],
    [
      new Spread(GeographicArea.riverfronts, Rarity.rare),
      new Spread(GeographicArea.riverMeadow, Rarity.extremeRare),
      new Spread(GeographicArea.swamp, Rarity.sometimes),
    ],
    Modificator["-6"],
    [
      {
        amount: new Amount(1),
        plantPart: plantParts["Donf-Stängel"],
      },
    ],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
      AventuricMonth.firun,
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
    ],
    [new Reference(Book.zba3, [234])],
    undefined,
    undefined
  ),

  Dornrose: new Plant(
    true,
    "Dornrose",
    "Kontext",
    "Beschreibung",
    [PlantType.supernatural],
    [AventuricArea.moghulatDron],
    [
      new Spread(GeographicArea.grassland, Rarity.common),
      new Spread(GeographicArea.forest, Rarity.sometimes),
      new Spread(GeographicArea.cultureLand, Rarity.sometimes),
    ],
    Modificator["-3"],
    [
      {
        amount: new Amount(1, 1, DiceType.d6),
        plantPart: plantParts["Dornrosen Blüte"],
      },
      {
        amount: new Amount(undefined, 5, DiceType.d6),
        plantPart: plantParts["Dornrosen Ranke"],
      },
    ],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
      AventuricMonth.firun,
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
    ],
    [new Reference(Book.zba3, [235])],
    undefined,
    undefined
  ),

  Efeuer: new Plant(
    true,
    "Efeuer",
    "Kontext",
    "Beschreibung",
    [PlantType.dangerous],
    [AventuricArea.northAventurien, AventuricArea.middleAventurien],
    [
      new Spread(GeographicArea.forest, Rarity.rare),
      new Spread(GeographicArea.caveHumid, Rarity.sometimes),
      new Spread(GeographicArea.caveDry, Rarity.sometimes),
      new Spread(GeographicArea.ruins, Rarity.sometimes),
    ],
    Modificator["-4"],
    [],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
      AventuricMonth.firun,
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
    ],
    [new Reference(Book.zba3, [235])],
    undefined,
    undefined
  ),

  Egelschreck: new Plant(
    true,
    "Egelschreck",
    "Kontext",
    "Beschreibung",
    [PlantType.crop],
    [AventuricArea.middleAventurien],
    [
      new Spread(GeographicArea.swamp, Rarity.common),
      new Spread(GeographicArea.grasslandsHumid, Rarity.sometimes),
      new Spread(GeographicArea.forestEdge, Rarity.rare),
    ],
    Modificator["-6"],
    [
      {
        amount: new Amount(undefined, 20, DiceType.d20),
        plantPart: plantParts["Egelschreck-Blätter"],
      },
    ],
    [AventuricMonth.rondra, AventuricMonth.efferd],
    [new Reference(Book.zba3, [235])],
    undefined,
    undefined
  ),

  "Eitriger Krötenschemel": new Plant(
    true,
    "Eitriger Krötenschemel",
    "Kontext",
    "Beschreibung",
    [PlantType.poison],
    [AventuricArea.allSouthYellowSickle],
    [
      new Spread(GeographicArea.swamp, Rarity.sometimes),
      new Spread(GeographicArea.forest, Rarity.extremeRare),
      new Spread(GeographicArea.riverMeadow, Rarity.rare),
    ],
    Modificator["-2"],
    [
      {
        amount: new Amount(undefined, 2, DiceType.d6),
        plantPart: plantParts["Eitriger Krötenschemel Pilzhaut"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [236])],
    undefined,
    undefined
  ),

  "Feuermoos und Efferdsmoos": new Plant(
    true,
    "Feuermoos und Efferdsmoos",
    "Kontext",
    "Beschreibung",
    [PlantType.dangerous],
    [AventuricArea.northAventurien, AventuricArea.middleAventurien],
    [new Spread(GeographicArea.caveHumid, Rarity.sometimes)],
    Modificator["-15"],
    [
      {
        amount: new Amount(undefined, undefined, undefined),
        plantPart: plantParts["Mineral von Feuer- und Efferdsmoos"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [236])],
    undefined,
    undefined
  ),

  Feuerschlick: new Plant(
    true,
    "Feuerschlick",
    "Kontext",
    "Beschreibung",
    [PlantType.crop, PlantType.heal],
    [AventuricArea.coastalElburien],
    [],
    Modificator["-6"],
    [],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [237])],
    (
      month?: AventuricMonth,
      daytime?: Daytime,
      moonPhase?: Moonphase
    ): Modificator => {
      if (
        daytime === Daytime.night &&
        moonPhase === Moonphase.fullMoon &&
        [AventuricMonth.efferd, AventuricMonth.rondra].includes(month)
      )
        return Modificator["5"];
      return Modificator["-6"];
    },
    (
      month?: AventuricMonth,
      tapStar?: number,
      moonPhase?: Moonphase,
      daytime?: Daytime
    ): HarvestAmount[] => {
      if (
        [AventuricMonth.efferd, AventuricMonth.rondra].includes(month) &&
        (!moonPhase || moonPhase === Moonphase.fullMoon) &&
        (!daytime || daytime === Daytime.night)
      ) {
        return [
          {
            amount: new Amount(undefined, 1, DiceType.d6, Unit.stone),
            plantPart: plantParts["Feuerschlick Algen (leuchtend)"],
          },
        ];
      }

      return [
        {
          amount: new Amount(undefined, 1, DiceType.d6, Unit.stone),
          plantPart: plantParts["Feuerschlick Algen"],
        },
      ];
    },
    (month?: AventuricMonth): Spread[] => {
      const spreads = [new Spread(GeographicArea.ocean, Rarity.extremeRare)];
      if ([AventuricMonth.efferd, AventuricMonth.rondra].includes(month))
        spreads.push(
          new Spread(GeographicArea.tidalZone, Rarity.extremeCommon)
        );
      else spreads.push(new Spread(GeographicArea.tidalZone, Rarity.common));
      return spreads;
    }
  ),

  Finage: new Plant(
    true,
    "Finage",
    "Kontext",
    "Beschreibung",
    [PlantType.heal],
    [AventuricArea.southAventurienNeethaThalusa],
    [
      new Spread(GeographicArea.forest, Rarity.rare),
      new Spread(GeographicArea.rainForest, Rarity.rare),
      new Spread(GeographicArea.grassland, Rarity.extremeRare),
    ],
    Modificator["-5"],
    [],
    [
      AventuricMonth.boron,
      AventuricMonth.hesinde,
      AventuricMonth.firun,
      AventuricMonth.peraine,
    ],
    [new Reference(Book.zba3, [238])],
    undefined,
    (month?: AventuricMonth): HarvestAmount[] => {
      if (month === AventuricMonth.peraine) {
        return [
          {
            amount: new Amount(undefined, 1, DiceType.d20, Unit.count),
            plantPart: plantParts["Finage Triebe"],
          },
        ];
      }

      return [
        {
          amount: new Amount(undefined, 1, DiceType.d6, Unit.stone),
          plantPart: plantParts["Finage Bast"],
        },
      ];
    },
    undefined
  ),

  "Grüne Schleimschlange": new Plant(
    true,
    "Grüne Schleimschlange",
    "Kontext",
    "Beschreibung",
    [PlantType.dangerous],
    [
      AventuricArea.orcLand,
      AventuricArea.svelltSwamps,
      AventuricArea.deathSwamp,
    ],
    [
      new Spread(GeographicArea.swamp, Rarity.common),
      new Spread(GeographicArea.riverMeadow, Rarity.extremeRare),
    ],
    Modificator["-4"],
    [],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [238])],
    undefined,
    undefined,
    undefined
  ),

  Gulmond: new Plant(
    true,
    "Gulmond",
    "Kontext",
    "Beschreibung",
    [PlantType.crop],
    [AventuricArea.northAventurienNostriaVallusa],
    [
      new Spread(GeographicArea.steppe, Rarity.common),
      new Spread(GeographicArea.forest, Rarity.sometimes),
      new Spread(GeographicArea.highland, Rarity.sometimes),
    ],
    Modificator["-6"],
    [
      {
        amount: new Amount(undefined, 2, DiceType.d6),
        plantPart: plantParts["Gulmond Blatt"],
      },
    ],
    [
      ...plant_harvest_timeframe_spring,
      ...plant_harvest_timeframe_summer,
      ...plant_harvest_timeframe_autumn,
    ],
    [new Reference(Book.zba3, [238])],
    undefined,
    undefined,
    undefined
  ),

  Hiradwurz: new Plant(
    true,
    "Hiradwurz",
    "Kontext",
    "Beschreibung",
    [PlantType.crop, PlantType.heal],
    [
      AventuricArea.gorien,
      AventuricArea.aranien,
      AventuricArea.mhanadistan,
      AventuricArea.amhalassih,
    ],
    [new Spread(GeographicArea.steppe, Rarity.rare)],
    Modificator["0"],
    [
      {
        amount: new Amount(1),
        plantPart: plantParts["Hiradwurz Wurzel"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [239])],
    (month?: AventuricMonth): Modificator => {
      if (
        [
          AventuricMonth.rahja,
          AventuricMonth.namelessDays,
          AventuricMonth.praios,
          AventuricMonth.efferd,
          AventuricMonth.travia,
        ].includes(month)
      )
        return Modificator["-5"];
      return Modificator["-8"];
    },
    undefined,
    undefined
  ),

  Hollbeere: new Plant(
    true,
    "Hollbeere",
    "Kontext",
    "",
    [PlantType.poison, PlantType.heal],
    [AventuricArea.albernia, AventuricArea.nostria],
    [
      new Spread(GeographicArea.forestEdge, Rarity.common),
      new Spread(GeographicArea.forest, Rarity.sometimes),
    ],
    Modificator["-4"],
    [
      {
        amount: new Amount(3, 2, DiceType.d6),
        plantPart: plantParts["Hollbeere Blatt"],
        factor: new Amount(undefined, 2, DiceType.d6),
      },
      {
        amount: new Amount(5, 2, DiceType.d6),
        plantPart: plantParts["Hollbeere Beere"],
        factor: new Amount(undefined, 2, DiceType.d6),
      },
    ],
    [AventuricMonth.rondra, AventuricMonth.efferd],
    [new Reference(Book.zba3, [240])],
    undefined,
    undefined,
    undefined
  ),

  Höllenkraut: new Plant(
    true,
    "Höllenkraut",
    "Kontext",
    "",
    [PlantType.poison, PlantType.crop],
    [AventuricArea.forestIslands, AventuricArea.southWestsouthMengbilla],
    [
      new Spread(GeographicArea.rainForest, Rarity.common),
      new Spread(GeographicArea.forest, Rarity.rare),
      new Spread(GeographicArea.forestEdge, Rarity.extremeRare),
    ],
    Modificator["-8"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d20, Unit.halfStone),
        plantPart: plantParts["Höllenkraut Ranke"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [240])],
    undefined,
    undefined,
    undefined
  ),

  Horusche: new Plant(
    true,
    "Horusche",
    "Kontext",
    "",
    [PlantType.poison, PlantType.crop],
    [AventuricArea.southMaraskan],
    [new Spread(GeographicArea.forestHumid, Rarity.rare)],
    Modificator["-8"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d3, Unit.count),
        plantPart: plantParts["Horusche Kern"],
        factor: new Amount(undefined, 1, DiceType.d6),
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [240])],
    undefined,
    undefined,
    undefined
  ),

  Ilmenblatt: new Plant(
    true,
    "Ilmenblatt",
    "Kontext",
    "",
    [PlantType.crop],
    [
      AventuricArea.westAventurien,
      AventuricArea.cyclopeIslands,
      AventuricArea.altroun,
    ],
    [
      new Spread(GeographicArea.forestClearing, Rarity.sometimes),
      new Spread(GeographicArea.grassland, Rarity.sometimes),
      new Spread(GeographicArea.mountainSlope, Rarity.extremeRare),
    ],
    Modificator["-2"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d20, Unit.count),
        plantPart: plantParts["Ilmenblatt Blatt und Blüte"],
      },
    ],
    [AventuricMonth.travia, AventuricMonth.ingerimm],
    [new Reference(Book.zba3, [240])],
    undefined,
    undefined,
    undefined
  ),

  Iribaarslilie: new Plant(
    true,
    "Iribaarslilie",
    "Kontext",
    "",
    [PlantType.supernatural],
    [AventuricArea.all],
    [new Spread(GeographicArea.swamp, Rarity.extremeRare)],
    Modificator["-12"],
    [],
    [],
    [new Reference(Book.zba3, [242])],
    undefined,
    undefined,
    undefined
  ),

  Jagdgras: new Plant(
    true,
    "Jagdgras",
    "Kontext",
    "",
    [PlantType.dangerous],
    [AventuricArea.maraskan, AventuricArea.aranien, AventuricArea.greenPlane],
    [
      new Spread(GeographicArea.mountains, Rarity.sometimes),
      new Spread(GeographicArea.highland, Rarity.extremeRare),
      new Spread(GeographicArea.steppe, Rarity.extremeRare),
      new Spread(GeographicArea.forest, Rarity.extremeRare),
    ],
    Modificator["-15"],
    [],
    [],
    [new Reference(Book.zba3, [242])],
    undefined,
    undefined,
    undefined
  ),

  Joruga: new Plant(
    true,
    "Joruga",
    "Kontext",
    "",
    [PlantType.heal],
    [
      AventuricArea.nostria,
      AventuricArea.andergast,
      AventuricArea.albernia,
      AventuricArea.northMarks,
    ],
    [
      new Spread(GeographicArea.mountains, Rarity.extremeRare),
      new Spread(GeographicArea.grassland, Rarity.rare),
      new Spread(GeographicArea.forest, Rarity.sometimes),
    ],
    Modificator["-7"],
    [
      {
        amount: new Amount(1, undefined, undefined, undefined),
        plantPart: plantParts["Joruga Wurzel"],
      },
    ],
    [
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
    ],
    [new Reference(Book.zba3, [243])],
    undefined,
    undefined,
    undefined
  ),

  Kairan: new Plant(
    true,
    "Kairan",
    "Kontext",
    "",
    [PlantType.crop],
    [
      AventuricArea.lampreySea,
      AventuricArea.hermitSea,
      AventuricArea.magicWaterBodies,
    ],
    [new Spread(GeographicArea.riverfronts, Rarity.rare)],
    Modificator["0"],
    [
      {
        amount: new Amount(1, undefined, undefined, undefined),
        plantPart: plantParts["Kairan Halm"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [243])],
    (
      month?: AventuricMonth,
      daytime?: Daytime,
      moonPhase?: Moonphase,
      spreads?: Spread[],
      zfpStar?: number
    ): Modificator => {
      let mod = -6;
      if (zfpStar) mod += zfpStar * 2;
      return Modificator[String(mod)];
    },
    undefined,
    undefined
  ),

  Kajubo: new Plant(
    true,
    "Kajubo",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.altoum, AventuricArea.souram, AventuricArea.nikkali],
    [
      new Spread(GeographicArea.forestEdge, Rarity.rare),
      new Spread(GeographicArea.coastBeach, Rarity.rare),
    ],
    Modificator["-4"],
    [
      {
        amount: new Amount(undefined, 2, DiceType.d6, Unit.count),
        plantPart: plantParts["Kajubo Knospe"],
      },
    ],
    [
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
    ],
    [new Reference(Book.zba3, [244])],
    undefined,
    undefined,
    undefined
  ),

  "Khôm- oder Mhanadiknolle": new Plant(
    true,
    "Khôm- oder Mhanadiknolle",
    "Kontext",
    "",
    [PlantType.crop],
    [
      AventuricArea.gorien,
      AventuricArea.easternKhom,
      AventuricArea.mhanadistan,
    ],
    [
      new Spread(GeographicArea.desert, Rarity.extremeRare),
      new Spread(GeographicArea.steppe, Rarity.rare),
    ],
    Modificator["-12"],
    [
      {
        amount: new Amount(1),
        plantPart: plantParts["Khôm- oder Mhanadiknolle Wurzel"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [244])],
    (month?: AventuricMonth) => {
      if (
        [
          AventuricMonth.praios,
          AventuricMonth.rondra,
          AventuricMonth.efferd,
        ].includes(month)
      )
        return Modificator["-5"];
      return Modificator["-12"];
    },
    undefined,
    undefined
  ),

  Klippenzahn: new Plant(
    true,
    "Klippenzahn",
    "Kontext",
    "",
    [PlantType.heal],
    [AventuricArea.orcLand, AventuricArea.thorwal, AventuricArea.gjalkerWastes],
    [
      new Spread(GeographicArea.mountains, Rarity.sometimes),
      new Spread(GeographicArea.highland, Rarity.sometimes),
    ],
    Modificator["-8"],
    [
      {
        amount: new Amount(undefined, 2, DiceType.d6),
        plantPart: plantParts["Klippenzahn Stängel"],
      },
    ],
    [
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
    ],
    [new Reference(Book.zba3, [245])],
    undefined,
    undefined,
    undefined
  ),

  Kukuka: new Plant(
    true,
    "Kukuka",
    "Kontext",
    "",
    [PlantType.crop, PlantType.poison],
    [AventuricArea.aroundRainMountains],
    [new Spread(GeographicArea.rainForest, Rarity.rare)],
    Modificator["-10"],
    [
      {
        amount: new Amount(undefined, 20, DiceType.d3),
        plantPart: plantParts["Kukuka Blatt"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [245])],
    undefined,
    undefined,
    undefined
  ),

  Färberlotos: new Plant(
    true,
    "Färberlotos",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.allSouthHavenaPerricum],
    [
      new Spread(GeographicArea.ponds, Rarity.sometimes),
      new Spread(GeographicArea.riverfronts, Rarity.sometimes),
    ],
    Modificator["-9"],
    [
      {
        amount: new Amount(1, 2, DiceType.d6),
        plantPart: plantParts["Färberlotos Blüte"],
      },
    ],
    [
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
    ],
    [new Reference(Book.zba3, [246])],
    undefined,
    undefined,
    undefined
  ),

  "Purpurner Lotos": new Plant(
    true,
    "Purpurner Lotos",
    "Kontext",
    "",
    [PlantType.poison],
    [AventuricArea.all],
    [
      new Spread(GeographicArea.ponds, Rarity.sometimes),
      new Spread(GeographicArea.riverfronts, Rarity.sometimes),
    ],
    Modificator["-7"],
    [
      {
        amount: new Amount(1, 1, DiceType.d6),
        plantPart: plantParts["Purpurner Lotos Blüte"],
      },
    ],
    [
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
    ],
    [new Reference(Book.zba3, [246])],
    undefined,
    undefined,
    undefined
  ),

  "Schwarzer Lotos": new Plant(
    true,
    "Schwarzer Lotos",
    "Kontext",
    "",
    [PlantType.poison],
    [AventuricArea.all],
    [new Spread(GeographicArea.swamp, Rarity.extremeRare)],
    Modificator["-6"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d6),
        plantPart: plantParts["Schwarzer Lotos Blüte"],
      },
    ],
    [
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
    ],
    [new Reference(Book.zba3, [246])],
    undefined,
    undefined,
    undefined
  ),

  "Grauer Lotos": new Plant(
    true,
    "Grauer Lotos",
    "Kontext",
    "",
    [PlantType.supernatural],
    [AventuricArea.warunkai],
    [
      new Spread(GeographicArea.ponds, Rarity.rare),
      new Spread(GeographicArea.riverfronts, Rarity.rare),
      new Spread(GeographicArea.swamp, Rarity.extremeRare),
    ],
    Modificator["-8"],
    [
      {
        amount: new Amount(1, 1, DiceType.d6),
        plantPart: plantParts["Grauer Lotos Blüte"],
      },
    ],
    [
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
    ],
    [new Reference(Book.zba3, [246])],
    undefined,
    undefined,
    undefined
  ),

  "Weißer Lotos": new Plant(
    true,
    "Weißer Lotos",
    "Kontext",
    "",
    [PlantType.poison],
    [AventuricArea.all],
    [
      new Spread(GeographicArea.ponds, Rarity.rare),
      new Spread(GeographicArea.riverfronts, Rarity.rare),
      new Spread(GeographicArea.swamp, Rarity.extremeRare),
    ],
    Modificator["-10"],
    [
      {
        amount: new Amount(1, 1, DiceType.d6),
        plantPart: plantParts["Weißer Lotos Blüte"],
      },
    ],
    [
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
    ],
    [new Reference(Book.zba3, [247])],
    undefined,
    undefined,
    undefined
  ),

  "Weißgelber Lotos": new Plant(
    true,
    "Weißgelber Lotos",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.allSouthHavenaPerricum],
    [
      new Spread(GeographicArea.ponds, Rarity.rare),
      new Spread(GeographicArea.riverfronts, Rarity.rare),
    ],
    Modificator["-10"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d3),
        plantPart: plantParts["Weißgelber Lotos Blüte"],
      },
    ],
    [
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
    ],
    [new Reference(Book.zba3, [247])],
    undefined,
    undefined,
    undefined
  ),

  Lulanie: new Plant(
    true,
    "Lulanie",
    "Kontext",
    "",
    [PlantType.heal],
    [AventuricArea.middleAventurienTrallopPunin],
    [
      new Spread(GeographicArea.forest, Rarity.rare),
      new Spread(GeographicArea.forestEdge, Rarity.extremeRare),
    ],
    Modificator["-5"],
    [
      {
        amount: new Amount(1, undefined, undefined),
        plantPart: plantParts["Lulanie Blüte"],
      },
    ],
    [
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
    ],
    [new Reference(Book.zba3, [247])],
    undefined,
    undefined,
    undefined
  ),

  Madablüte: new Plant(
    true,
    "Madablüte",
    "Kontext",
    "",
    [PlantType.supernatural],
    [AventuricArea.northAventurien],
    [
      new Spread(GeographicArea.steppe, Rarity.extremeRare),
      new Spread(GeographicArea.mountains, Rarity.extremeRare),
    ],
    Modificator["-5"],
    [
      {
        amount: new Amount(1, undefined, undefined),
        plantPart: plantParts["Madablüte Blüte"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [247])],
    (
      month?: AventuricMonth,
      daytime?: Daytime,
      moonPhase?: Moonphase
    ): Modificator => {
      if (moonPhase === Moonphase.fullMoon && daytime === Daytime.night)
        return Modificator["-5"];
      return Modificator["-15"];
    },
    undefined,
    undefined
  ),

  Malomis: new Plant(
    true,
    "Malomis",
    "Kontext",
    "",
    [PlantType.supernatural],
    [AventuricArea.gardens],
    [new Spread(GeographicArea.garden, Rarity.extremeRare)],
    Modificator["-10"],
    [
      {
        amount: new Amount(1, undefined, undefined),
        plantPart: plantParts["Malomis Blüte"],
      },
    ],
    [AventuricMonth.rondra],
    [new Reference(Book.zba3, [248])],
    undefined,
    undefined,
    undefined
  ),

  "Menchal-Kaktus": new Plant(
    true,
    "Menchal-Kaktus",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.khomGoricDesert],
    [new Spread(GeographicArea.desert, Rarity.extremeRare)],
    Modificator["-4"],
    [],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [249])],
    undefined,
    (
      month?: AventuricMonth,
      tapStar?: number,
      moonPhase?: Moonphase,
      daytime?: Daytime,
      luckRoll?: number
    ): HarvestAmount[] => {
      const amount: HarvestAmount[] = [
        {
          amount: new Amount(undefined, 1, DiceType.d6, Unit.halfPint),
          plantPart: plantParts["Menchalsaft"],
        },
      ];
      if (luckRoll === 1)
        amount.push({
          amount: new Amount(undefined, 1, DiceType.d6),
          plantPart: plantParts["Menchal Blüte"],
        });
      return amount;
    },
    undefined
  ),

  "Merach-Strauch": new Plant(
    true,
    "Merach-Strauch",
    "Kontext",
    "",
    [PlantType.crop, PlantType.poison],
    [
      AventuricArea.mhanadistan,
      AventuricArea.aroundRainMountains,
      AventuricArea.thalusien,
      AventuricArea.unauMountains,
    ],
    [
      new Spread(GeographicArea.rainForest, Rarity.rare),
      new Spread(GeographicArea.forest, Rarity.extremeRare),
      new Spread(GeographicArea.garden, Rarity.extremeRare),
    ],
    Modificator["-2"],
    [
      {
        amount: new Amount(undefined, 2, DiceType.d20),
        plantPart: plantParts["Merach Frucht"],
      },
    ],
    [AventuricMonth.efferd, AventuricMonth.travia],
    [new Reference(Book.zba3, [249])],
    undefined,
    undefined,
    undefined
  ),

  Messergras: new Plant(
    true,
    "Messergras",
    "Kontext",
    "",
    [PlantType.crop, PlantType.dangerous],
    [AventuricArea.allRivaMengilla],
    [
      new Spread(GeographicArea.steppe, Rarity.rare),
      new Spread(GeographicArea.highland, Rarity.extremeRare),
      new Spread(GeographicArea.desert, Rarity.extremeRare),
    ],
    Modificator["-6"],
    [],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [250])],
    undefined,
    undefined,
    undefined
  ),

  Mibelrohr: new Plant(
    true,
    "Mibelrohr",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.westCoastSalzaMengbilla],
    [
      new Spread(GeographicArea.riverfronts, Rarity.sometimes),
      new Spread(GeographicArea.riverMeadow, Rarity.rare),
      new Spread(GeographicArea.swamp, Rarity.extremeRare),
    ],
    Modificator["-10"],
    [
      {
        amount: new Amount(undefined, 2, DiceType.d6),
        plantPart: plantParts["Mibelkolben und Stängel"],
      },
    ],
    [
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
    ],
    [new Reference(Book.zba3, [251])],
    undefined,
    undefined,
    undefined
  ),

  Mirbelstein: new Plant(
    true,
    "Mirbelstein",
    "Kontext",
    "",
    [PlantType.crop],
    [
      AventuricArea.garetia,
      AventuricArea.darpatia,
      AventuricArea.warunk,
      AventuricArea.beilunk,
    ],
    [
      new Spread(GeographicArea.forest, Rarity.common),
      new Spread(GeographicArea.grassland, Rarity.sometimes),
      new Spread(GeographicArea.highland, Rarity.rare),
    ],
    Modificator["-8"],
    [
      {
        amount: new Amount(undefined, 2, DiceType.d6),
        plantPart: plantParts["Mirbelwurzelknolle"],
      },
    ],
    [
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
    ],
    [new Reference(Book.zba3, [251])],
    undefined,
    undefined,
    undefined
  ),

  "Mirhamer Seidenliane": new Plant(
    true,
    "Mirhamer Seidenliane",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.aroundRainMountains],
    [
      new Spread(GeographicArea.rainForest, Rarity.rare),
      new Spread(GeographicArea.mountainSlope, Rarity.extremeRare),
      new Spread(GeographicArea.riverfronts, Rarity.extremeRare),
    ],
    Modificator["-4"],
    [
      {
        amount: new Amount(1, undefined, undefined),
        plantPart: plantParts["Mirhamer Seidenliane Ranke"],
      },
      {
        amount: new Amount(1, 1, DiceType.d2),
        plantPart: plantParts["Mirhamer Seidenliane Knoten"],
      },
    ],
    [
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
    ],
    [new Reference(Book.zba3, [251])],
    undefined,
    (month?: AventuricMonth): HarvestAmount[] => {
      const amount: HarvestAmount[] = [
        {
          amount: new Amount(1, undefined, undefined),
          plantPart: plantParts["Mirhamer Seidenliane Ranke"],
        },
      ];

      if (
        [
          AventuricMonth.tsa,
          AventuricMonth.phex,
          AventuricMonth.peraine,
          AventuricMonth.ingerimm,
        ].includes(month)
      )
        amount.push({
          amount: new Amount(1, 1, DiceType.d2),
          plantPart: plantParts["Mirhamer Seidenliane Knoten"],
        });

      return amount;
    },
    undefined
  ),

  Bleichmohn: new Plant(
    true,
    "Bleichmohn",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.all],
    [new Spread(GeographicArea.mountains, Rarity.rare)],
    Modificator["-5"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d6),
        plantPart: plantParts["Bleichmohn Samenkapsel"],
      },
    ],
    [AventuricMonth.rondra],
    [new Reference(Book.zba3, [252])],
    undefined,
    undefined,
    undefined
  ),

  "Bunter Mohn": new Plant(
    true,
    "Bunter Mohn",
    "Kontext",
    "",
    [PlantType.crop],
    [
      AventuricArea.northAventurien,
      AventuricArea.middleAventurien,
      AventuricArea.aranien,
      AventuricArea.lovelyField,
    ],
    [
      new Spread(GeographicArea.grassland, Rarity.common),
      new Spread(GeographicArea.riverfronts, Rarity.sometimes),
      new Spread(GeographicArea.forestEdge, Rarity.rare),
    ],
    Modificator["-5"],
    [
      {
        amount: new Amount(1),
        plantPart: plantParts["Bunter Mohn Samenkapsel"],
      },
    ],
    [AventuricMonth.travia],
    [new Reference(Book.zba3, [252])],
    undefined,
    undefined,
    undefined
  ),

  "Grauer Mohn": new Plant(
    true,
    "Grauer Mohn",
    "Kontext",
    "",
    [PlantType.crop, PlantType.poison],
    [AventuricArea.all],
    [new Spread(GeographicArea.mountains, Rarity.extremeRare)],
    Modificator["-1"],
    [],
    [AventuricMonth.travia],
    [new Reference(Book.zba3, [253])],
    undefined,
    (month?: AventuricMonth): HarvestAmount[] => {
      const amount: HarvestAmount[] = [
        {
          amount: new Amount(1),
          plantPart: plantParts["Grauer Mohn Blüte"],
        },
      ];
      if ([AventuricMonth.rondra].includes(month))
        amount.push({
          amount: new Amount(1),
          plantPart: plantParts["Grauer Mohn Samenkapsel"],
        });
      return amount;
    },
    undefined
  ),

  Purpurmohn: new Plant(
    true,
    "Purpurmohn",
    "Kontext",
    "",
    [PlantType.supernatural],
    [
      AventuricArea.lovelyField,
      AventuricArea.almada,
      AventuricArea.aranien,
      AventuricArea.southAventurien,
    ],
    [
      new Spread(GeographicArea.grassland, Rarity.rare),
      new Spread(GeographicArea.forestClearing, Rarity.extremeRare),
      new Spread(GeographicArea.riverfronts, Rarity.extremeRare),
    ],
    Modificator["-3"],
    [
      {
        amount: new Amount(1),
        plantPart: plantParts["Purpurmohn Samenkapsel"],
      },
    ],
    [AventuricMonth.rahja],
    [new Reference(Book.zba3, [253])],
    undefined,
    undefined,
    undefined
  ),

  "Schwarzer Mohn": new Plant(
    true,
    "Schwarzer Mohn",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.cyclopeIslands],
    [new Spread(GeographicArea.palakarRuins, Rarity.extremeCommon)],
    Modificator["-5"],
    [],
    [AventuricMonth.efferd, AventuricMonth.travia, AventuricMonth.boron],
    [new Reference(Book.zba3, [253])],
    undefined,
    (month?: AventuricMonth): HarvestAmount[] => {
      const amount: HarvestAmount[] = [
        {
          amount: new Amount(2),
          plantPart: plantParts["Schwarzer Mohn Blatt"],
        },
      ];
      if ([AventuricMonth.boron].includes(month))
        amount.push({
          amount: new Amount(1),
          plantPart: plantParts["Schwarzer Mohn Samenkapsel"],
        });
      return amount;
    },
    undefined
  ),

  Tigermohn: new Plant(
    true,
    "Tigermohn",
    "Kontext",
    "",
    [PlantType.crop],
    [
      AventuricArea.northAventurien,
      AventuricArea.middleAventurien,
      AventuricArea.lovelyField,
      AventuricArea.aranien,
    ],
    [
      new Spread(GeographicArea.grassland, Rarity.rare),
      new Spread(GeographicArea.riverMeadow, Rarity.rare),
      new Spread(GeographicArea.forestEdge, Rarity.rare),
    ],
    Modificator["-10"],
    [
      {
        amount: new Amount(1),
        plantPart: plantParts["Tigermohn Samenkapsel"],
      },
    ],
    [AventuricMonth.travia],
    [new Reference(Book.zba3, [254])],
    undefined,
    undefined,
    undefined
  ),

  Morgendornstrauch: new Plant(
    true,
    "Morgendornstrauch",
    "Kontext",
    "",
    [PlantType.dangerous],
    [AventuricArea.northEastAventurienLettaTobimora],
    [new Spread(GeographicArea.swamp, Rarity.rare)],
    Modificator["-13"],
    [],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [254])],
    undefined,
    undefined,
    undefined
  ),

  Naftanstaude: new Plant(
    true,
    "Naftanstaude",
    "Kontext",
    "",
    [PlantType.poison],
    [AventuricArea.allSouthRiva],
    [
      new Spread(GeographicArea.grassland, Rarity.rare),
      new Spread(GeographicArea.steppe, Rarity.extremeRare),
      new Spread(GeographicArea.forestEdge, Rarity.extremeRare),
    ],
    Modificator["-1"],
    [
      {
        amount: new Amount(1),
        plantPart: plantParts["Naftanstaude Saft"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [255])],
    undefined,
    undefined,
    undefined
  ),

  Neckerkraut: new Plant(
    true,
    "Neckerkraut",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.westCoast],
    [
      new Spread(GeographicArea.coastBeach, Rarity.sometimes),
      new Spread(GeographicArea.grassland, Rarity.rare),
      new Spread(GeographicArea.swamp, Rarity.extremeRare),
    ],
    Modificator["-4"],
    [
      {
        amount: new Amount(5, 1, DiceType.d20),
        plantPart: plantParts["Neckerkraut Blatt"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [255])],
    undefined,
    undefined,
    undefined
  ),

  Nothilf: new Plant(
    true,
    "Nothilf",
    "Kontext",
    "",
    [PlantType.heal],
    [AventuricArea.salamanderForest],
    [
      new Spread(GeographicArea.forest, Rarity.rare),
      new Spread(GeographicArea.mountains, Rarity.rare),
    ],
    Modificator["-6"],
    [],
    [AventuricMonth.praios, AventuricMonth.peraine],
    [new Reference(Book.zba3, [256])],
    undefined,
    (month?: AventuricMonth): HarvestAmount[] => {
      if ([AventuricMonth.praios].includes(month))
        return [
          {
            amount: new Amount(10, 2, DiceType.d20),
            plantPart: plantParts["Nothilf Blatt"],
          },
        ];
      return [
        {
          amount: new Amount(2, 1, DiceType.d20),
          plantPart: plantParts["Nothilf Blüte"],
        },
      ];
    },
    undefined
  ),

  Olginwurz: new Plant(
    true,
    "Olginwurz",
    "Kontext",
    "",
    [PlantType.heal],
    [AventuricArea.rashtulswall],
    [
      new Spread(GeographicArea.highland, Rarity.rare),
      new Spread(GeographicArea.mountains, Rarity.rare),
      new Spread(GeographicArea.forest, Rarity.rare),
    ],
    Modificator["-6"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d3),
        plantPart: plantParts["Olginwurz Moosballen"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [256])],
    undefined,
    undefined,
    undefined
  ),

  Orazal: new Plant(
    true,
    "Orazal",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.southAventurienMengbilla],
    [
      new Spread(GeographicArea.rainForest, Rarity.rare),
      new Spread(GeographicArea.forest, Rarity.extremeRare),
    ],
    Modificator["-4"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d6),
        plantPart: plantParts["Orazal verholzter Stängel"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [257])],
    undefined,
    undefined,
    undefined
  ),

  "Orkland-Bovist": new Plant(
    true,
    "Orkland-Bovist",
    "Kontext",
    "",
    [PlantType.dangerous],
    [AventuricArea.orcLand, AventuricArea.svelltTal, AventuricArea.andergast],
    [
      new Spread(GeographicArea.highland, Rarity.rare),
      new Spread(GeographicArea.forest, Rarity.extremeRare),
      new Spread(GeographicArea.steppe, Rarity.extremeRare),
    ],
    Modificator["-4"],
    [],
    [
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
    ],
    [new Reference(Book.zba3, [258])],
    undefined,
    undefined,
    undefined
  ),

  Pestsporenpilz: new Plant(
    true,
    "Pestsporenpilz",
    "Kontext",
    "",
    [PlantType.crop, PlantType.dangerous],
    [AventuricArea.northAventurienNostriaVallusa],
    [
      new Spread(GeographicArea.swamp, Rarity.rare),
      new Spread(GeographicArea.grassland, Rarity.rare),
      new Spread(GeographicArea.forest, Rarity.extremeRare),
    ],
    Modificator["-6"],
    [
      {
        amount: new Amount(1),
        plantPart: plantParts["Pestsporenpilz Pilzhaut"],
      },
    ],
    [
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
    ],
    [new Reference(Book.zba3, [258])],
    undefined,
    undefined,
    undefined
  ),

  Phosphorpilz: new Plant(
    true,
    "Phosphorpilz",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.finsterkamm, AventuricArea.orcLand],
    [
      new Spread(GeographicArea.caveHumid, Rarity.sometimes),
      new Spread(GeographicArea.caveDry, Rarity.rare),
    ],
    Modificator["-6"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d6, Unit.stone),
        plantPart: plantParts["Phosphorpilzgeflecht"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [258])],
    undefined,
    undefined,
    undefined
  ),

  Quasselwurz: new Plant(
    true,
    "Quasselwurz",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.salamanderForest, AventuricArea.ongaloMountains],
    [new Spread(GeographicArea.forest, Rarity.rare)],
    Modificator["-12"],
    [
      {
        amount: new Amount(1),
        plantPart: plantParts["Quasselwurz Wurzel"],
      },
    ],
    [
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
    ],
    [new Reference(Book.zba3, [259])],
    undefined,
    undefined,
    undefined
  ),

  Quinja: new Plant(
    true,
    "Quinja",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.southLochHarodrol],
    [
      new Spread(GeographicArea.rainForest, Rarity.common),
      new Spread(GeographicArea.forest, Rarity.sometimes),
      new Spread(GeographicArea.forestEdge, Rarity.rare),
    ],
    Modificator["-6"],
    [
      {
        amount: new Amount(2, 1, DiceType.d3),
        plantPart: plantParts["Quinja Beere"],
      },
    ],
    [
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
    ],
    [new Reference(Book.zba3, [259])],
    undefined,
    undefined,
    undefined
  ),

  Rahjalieb: new Plant(
    true,
    "Rahjalieb",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.allSouthFestum],
    [
      new Spread(GeographicArea.forest, Rarity.sometimes),
      new Spread(GeographicArea.grassland, Rarity.sometimes),
      new Spread(GeographicArea.swamp, Rarity.sometimes),
      new Spread(GeographicArea.rainForest, Rarity.common),
    ],
    Modificator["-5"],
    [
      {
        amount: new Amount(undefined, 2, DiceType.d6),
        plantPart: plantParts["Rahjalieb Blatt"],
      },
    ],
    [
      AventuricMonth.firun,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
      AventuricMonth.hesinde,
    ],
    [new Reference(Book.zba3, [260])],
    undefined,
    undefined,
    undefined
  ),

  Rattenpilz: new Plant(
    true,
    "Rattenpilz",
    "Kontext",
    "",
    [PlantType.supernatural],
    [AventuricArea.all],
    [
      ...getAllSpreadsExcept(
        [
          GeographicArea.mountains,
          GeographicArea.mountainCaves,
          GeographicArea.mountainSlope,
          GeographicArea.desert,
          GeographicArea.ice,
        ],
        Rarity.extremeRare
      ),
      new Spread(GeographicArea.namelessTemple, Rarity.common),
    ],
    Modificator["-7"],
    [
      {
        amount: new Amount(1),
        plantPart: plantParts["Rattenpilz Pilz"],
      },
    ],
    [
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
    ],
    [new Reference(Book.zba3, [260])],
    undefined,
    undefined,
    undefined
  ),

  Rauschgurke: new Plant(
    true,
    "Rauschgurke",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.maraskan, AventuricArea.khunchoms],
    [
      new Spread(GeographicArea.forest, Rarity.sometimes),
      new Spread(GeographicArea.forestEdge, Rarity.rare),
    ],
    Modificator["-3"],
    [
      {
        amount: new Amount(undefined, 3, DiceType.d6),
        plantPart: plantParts["Rauschgurke Gurke"],
      },
    ],
    [
      AventuricMonth.tsa,
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
      AventuricMonth.boron,
    ],
    [new Reference(Book.zba3, [261])],
    undefined,
    undefined,
    undefined
  ),

  "Rote Pfeilblüte": new Plant(
    true,
    "Rote Pfeilblüte",
    "Kontext",
    "",
    [PlantType.heal],
    [AventuricArea.allSouthNeethaThalusa],
    [
      new Spread(GeographicArea.forest, Rarity.extremeRare),
      new Spread(GeographicArea.rainForest, Rarity.sometimes),
      new Spread(GeographicArea.coastBeach, Rarity.sometimes),
    ],
    Modificator["-7"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d6),
        plantPart: plantParts["Rote Pfeilblüte Blüte"],
      },
    ],
    [AventuricMonth.peraine, AventuricMonth.ingerimm, AventuricMonth.rahja],
    [new Reference(Book.zba3, [261])],
    undefined,
    undefined,
    undefined
  ),

  "Roter Drachenschlund": new Plant(
    true,
    "Roter Drachenschlund",
    "Kontext",
    "",
    [PlantType.heal],
    [AventuricArea.allNorthDrolThalusa],
    [
      new Spread(GeographicArea.forestEdge, Rarity.extremeRare),
      new Spread(GeographicArea.riverfronts, Rarity.extremeRare),
    ],
    Modificator["-10"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d6),
        plantPart: plantParts["Roter Drachenschlund Blatt"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [262])],
    (month?: AventuricMonth): Modificator => {
      if ([AventuricMonth.ingerimm, AventuricMonth.rahja].includes(month))
        return Modificator["-3"];
      return Modificator["-10"];
    },
    undefined,
    undefined
  ),

  Sansaro: new Plant(
    true,
    "Sansaro",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.pearlSeaSelemCharypso],
    [
      new Spread(GeographicArea.ocean, Rarity.extremeRare),
      new Spread(GeographicArea.riverfronts, Rarity.extremeRare),
    ],
    Modificator["-12"],
    [
      {
        amount: new Amount(0.5, undefined, undefined, Unit.stone),
        plantPart: plantParts["Sansaro Algen"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [262])],
    undefined,
    undefined,
    (month?: AventuricMonth): Spread[] => {
      const spreads = [new Spread(GeographicArea.ocean, Rarity.extremeRare)];
      if (
        [
          AventuricMonth.hesinde,
          AventuricMonth.firun,
          AventuricMonth.tsa,
        ].includes(month)
      )
        spreads.push(new Spread(GeographicArea.coastBeach, Rarity.rare));
      else spreads.push(new Spread(GeographicArea.coastBeach, Rarity.common));
      return spreads;
    }
  ),

  Satuariensbusch: new Plant(
    true,
    "Satuariensbusch",
    "Kontext",
    "",
    [PlantType.heal, PlantType.supernatural],
    [AventuricArea.allSouthYellowSickle],
    [
      new Spread(GeographicArea.forestEdge, Rarity.sometimes),
      new Spread(GeographicArea.forest, Rarity.sometimes),
    ],
    Modificator["2"],
    [],
    [
      AventuricMonth.phex,
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.efferd,
      AventuricMonth.travia,
    ],
    [
      new Reference(Book.zba3, [263]),
      new Reference(Book.wdg, [257]),
      new Reference(Book.srd, [117]),
      new Reference(Book.wda, [191]),
    ],
    undefined,
    (month?: AventuricMonth): HarvestAmount[] => {
      const amount: HarvestAmount[] = [];
      if (
        [
          AventuricMonth.ingerimm,
          AventuricMonth.rahja,
          AventuricMonth.namelessDays,
          AventuricMonth.praios,
        ].includes(month)
      )
        amount.push({
          amount: new Amount(undefined, 4, DiceType.d20, Unit.count),
          plantPart: plantParts["Satuariensbusch Blatt"],
        });
      if ([AventuricMonth.ingerimm, AventuricMonth.rahja].includes(month))
        amount.push({
          amount: new Amount(undefined, 1, DiceType.d20, Unit.count),
          plantPart: plantParts["Satuariensbusch Blüte"],
        });
      if ([AventuricMonth.efferd, AventuricMonth.travia].includes(month))
        amount.push({
          amount: new Amount(undefined, 1, DiceType.d20, Unit.count),
          plantPart: plantParts["Satuariensbusch Frucht"],
        });
      if (
        [
          AventuricMonth.phex,
          AventuricMonth.peraine,
          AventuricMonth.ingerimm,
          AventuricMonth.rahja,
          AventuricMonth.namelessDays,
          AventuricMonth.praios,
        ].includes(month)
      )
        amount.push({
          amount: new Amount(undefined, 1, DiceType.d3, Unit.flux),
          plantPart: plantParts["Satuariensbusch Saft"],
        });
      return amount;
    },
    undefined
  ),

  Schlangenzünglein: new Plant(
    true,
    "Schlangenzünglein",
    "Kontext",
    "",
    [PlantType.crop],
    [
      AventuricArea.maraskan,
      AventuricArea.perricumGulf,
      AventuricArea.mhanadistan,
      AventuricArea.darpat,
      AventuricArea.radrom,
    ],
    [
      new Spread(GeographicArea.rainForest, Rarity.extremeRare),
      new Spread(GeographicArea.riverfronts, Rarity.rare),
      new Spread(GeographicArea.swamp, Rarity.rare),
    ],
    Modificator["-3"],
    [
      {
        amount: new Amount(1, undefined, undefined, Unit.count),
        plantPart: plantParts["Schlangenzünglein Saft"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [263])],
    undefined,
    undefined,
    undefined
  ),

  "Schleichender Tod": new Plant(
    true,
    "Schleichender Tod",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.southLochHarodrol],
    [
      new Spread(GeographicArea.rainForest, Rarity.extremeRare),
      new Spread(GeographicArea.riverfronts, Rarity.extremeRare),
    ],
    Modificator["-6"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d6, Unit.count),
        plantPart: plantParts["Schleichender Tod Blüte"],
      },
    ],
    [AventuricMonth.ingerimm, AventuricMonth.rahja],
    [new Reference(Book.zba3, [264])],
    undefined,
    undefined,
    undefined
  ),

  "Schleimiger Sumpfknöterich": new Plant(
    true,
    "Schleimiger Sumpfknöterich",
    "Kontext",
    "",
    [PlantType.poison],
    [AventuricArea.bornland, AventuricArea.northTobria],
    [
      new Spread(GeographicArea.forest, Rarity.rare),
      new Spread(GeographicArea.forestEdge, Rarity.extremeRare),
    ],
    Modificator["-3"],
    [
      {
        amount: new Amount(undefined, 2, DiceType.d6, Unit.count),
        plantPart: plantParts["Schleimiger Sumpfknöterich Pilz"],
      },
    ],
    [
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
    ],
    [new Reference(Book.zba3, [264])],
    undefined,
    undefined,
    undefined
  ),

  Schlinggras: new Plant(
    true,
    "Schlinggras",
    "Kontext",
    "",
    [PlantType.dangerous],
    [AventuricArea.northAventurien],
    [new Spread(GeographicArea.swamp, Rarity.rare)],
    Modificator["-12"],
    [],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [265]), new Reference(Book.ga, [121])],
    undefined,
    undefined,
    undefined
  ),

  Schwarmschwamm: new Plant(
    true,
    "Schwarmschwamm",
    "Kontext",
    "",
    [PlantType.supernatural],
    [AventuricArea.blackTobria],
    [new Spread(GeographicArea.riverMeadow, Rarity.extremeRare)],
    Modificator["-3"],
    [
      {
        amount: new Amount(undefined, 2, DiceType.d6, Unit.count),
        plantPart: plantParts["Schleimiger Sumpfknöterich Pilz"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [265])],
    undefined,
    (month?: AventuricMonth): HarvestAmount[] => {
      const amount = [
        {
          amount: new Amount(1, undefined, undefined, Unit.count),
          plantPart: plantParts["Schwarmschwamm Schwamm"],
        },
      ];
      if ([AventuricMonth.efferd].includes(month))
        amount.push({
          amount: new Amount(undefined, 1, DiceType.d2, Unit.count),
          plantPart: plantParts["Schwarmschwamm Samenkörper"],
        });
      return amount;
    },
    undefined
  ),

  "Schwarzer Wein": new Plant(
    true,
    "Schwarzer Wein",
    "Kontext",
    "",
    [PlantType.supernatural],
    [AventuricArea.moghulatDron],
    [
      new Spread(GeographicArea.forest, Rarity.common),
      new Spread(GeographicArea.hills, Rarity.sometimes),
      new Spread(GeographicArea.steppe, Rarity.sometimes),
    ],
    Modificator["-2"],
    [],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [266])],
    undefined,
    (month?: AventuricMonth, tapStar?: number): HarvestAmount[] => {
      const amount = [];
      if (tapStar && tapStar >= 7)
        amount.push({
          amount: new Amount(undefined, 7, DiceType.d6, Unit.count),
          plantPart: plantParts["Schwarzer Wein Traube"],
        });
      return amount;
    },
    undefined
  ),

  Shurinstrauch: new Plant(
    true,
    "Shurinstrauch",
    "Kontext",
    "",
    [PlantType.poison],
    [AventuricArea.allBetweenRivaAlAnfa],
    [
      new Spread(GeographicArea.steppe, Rarity.rare),
      new Spread(GeographicArea.grassland, Rarity.rare),
      new Spread(GeographicArea.forest, Rarity.extremeRare),
    ],
    Modificator["-2"],
    [],
    [
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
    ],
    [new Reference(Book.zba3, [267])],
    undefined,
    (month): HarvestAmount[] => {
      const amount: HarvestAmount[] = [
        {
          amount: new Amount(undefined, 1, DiceType.d20, Unit.count),
          plantPart: plantParts["Shurinstrauch Blatt"],
        },
      ];
      if (
        [
          AventuricMonth.ingerimm,
          AventuricMonth.rahja,
          AventuricMonth.namelessDays,
          AventuricMonth.praios,
          AventuricMonth.rondra,
        ].includes(month)
      )
        amount.push({
          amount: new Amount(undefined, 1, DiceType.d20, Unit.count),
          plantPart: plantParts["Shurinstrauch Knolle"],
        });
      return amount;
    },
    undefined
  ),

  Talaschin: new Plant(
    true,
    "Talaschin",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.khomGoricDesert],
    [
      new Spread(GeographicArea.mountains, Rarity.sometimes),
      new Spread(GeographicArea.desert, Rarity.extremeRare),
      new Spread(GeographicArea.ice, Rarity.rare),
    ],
    Modificator["-5"],
    [
      {
        amount: new Amount(undefined, 1, DiceType.d6, Unit.count),
        plantPart: plantParts["Talaschin Flechte"],
      },
    ],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [268])],
    undefined,
    undefined,
    undefined
  ),

  Tarnblatt: new Plant(
    true,
    "Tarnblatt",
    "Kontext",
    "",
    [PlantType.poison],
    [AventuricArea.maraskan],
    [
      new Spread(GeographicArea.rainForest, Rarity.rare),
      new Spread(GeographicArea.forest, Rarity.rare),
    ],
    Modificator["-8"],
    [],
    plant_harvest_timeframe_all,
    [new Reference(Book.zba3, [268]), new Reference(Book.srd, [107])],
    undefined,
    undefined,
    undefined
  ),

  Tarnele: new Plant(
    true,
    "Tarnele",
    "Kontext",
    "",
    [PlantType.heal],
    [AventuricArea.allBetweenGerasimMengbilla],
    [
      new Spread(GeographicArea.grassland, Rarity.common),
      new Spread(GeographicArea.riverMeadow, Rarity.common),
      new Spread(GeographicArea.forest, Rarity.sometimes),
      new Spread(GeographicArea.swamp, Rarity.sometimes),
      new Spread(GeographicArea.highland, Rarity.extremeRare),
    ],
    Modificator["-4"],
    [
      {
        amount: new Amount(1, undefined, undefined, Unit.count),
        plantPart: plantParts["Tarnele Pflanze"],
      },
    ],
    [
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
    ],
    [new Reference(Book.zba3, [268])],
    undefined,
    undefined,
    undefined
  ),

  Thonnys: new Plant(
    true,
    "Thonnys",
    "Kontext",
    "",
    [PlantType.crop],
    [AventuricArea.northAventurienThorwalFestum],
    [
      new Spread(GeographicArea.steppe, Rarity.extremeRare),
      new Spread(GeographicArea.forest, Rarity.extremeRare),
    ],
    Modificator["-12"],
    [
      {
        amount: new Amount(4, 1, DiceType.d6, Unit.count),
        plantPart: plantParts["Thonnys Blatt"],
      },
    ],
    [
      AventuricMonth.peraine,
      AventuricMonth.ingerimm,
      AventuricMonth.rahja,
      AventuricMonth.namelessDays,
      AventuricMonth.praios,
      AventuricMonth.rondra,
      AventuricMonth.efferd,
      AventuricMonth.travia,
    ],
    [new Reference(Book.zba3, [269])],
    (month): Modificator => {
      if ([AventuricMonth.ingerimm, AventuricMonth.rahja].includes(month))
        return Modificator["-5"];
      return Modificator["-12"];
    },
    undefined,
    undefined
  ),
};
