import { Equipment, Price, Weight, Reference, Book } from "@/schemas";

export const equipment: Record<string, Equipment> = {
  Alkohol: new Equipment(
    "Alkohol",
    new Price(undefined, 1),
    new Weight(undefined, 1),
    []
  ),
  Salz: new Equipment(
    "Salz",
    new Price(undefined, 1),
    new Weight(undefined, 1),
    []
  ),
  Kalk: new Equipment(
    "Kalk",
    new Price(undefined, 1),
    new Weight(undefined, 1),
    [new Reference(Book.zba3, [235])]
  ),
  Fett: new Equipment(
    "Fett",
    new Price(undefined, 1),
    new Weight(undefined, 1),
    [new Reference(Book.zba3, [235])]
  ),
  Nussöl: new Equipment(
    "Nussöl",
    new Price(undefined, 1),
    new Weight(undefined, 1),
    [new Reference(Book.zba3, [236])]
  ),
  Meerwasser: new Equipment(
    "Meerwasser",
    new Price(undefined, 1),
    new Weight(undefined, 1),
    [new Reference(Book.zba3, [237])]
  ),
  Minze: new Equipment(
    "Minze",
    new Price(undefined, 1),
    new Weight(undefined, 1),
    [new Reference(Book.zba3, [239])]
  ),
  Vitriol: new Equipment(
    "Vitriol",
    new Price(undefined, 1),
    new Weight(undefined, 1),
    [new Reference(Book.zba3, [239])]
  ),
};
