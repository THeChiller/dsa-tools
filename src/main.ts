import { createApp } from "vue";
import { createPinia } from "pinia";

import DSATools from "./views/DSATools.vue";
import router from "./router";

const app = createApp(DSATools);

app.use(createPinia());
app.use(router);

app.mount("#app");
