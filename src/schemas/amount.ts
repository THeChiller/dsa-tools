export class DiceType {
  public static readonly d2 = new DiceType("W2", 1.5);
  public static readonly d3 = new DiceType("W3", 2);
  public static readonly d4 = new DiceType("W4", 2.5);
  public static readonly d6 = new DiceType("W6", 3.5);
  public static readonly d20 = new DiceType("W20", 10.5);
  private constructor(
    public readonly str: string,
    public readonly average: number
  ) {}
}

export enum Unit {
  count = "Anzahl",
  flux = "Flux",
  stone = "Stein",
  halfStone = "halbe Stein",
  halfPint = "halbe Maß",
  ounce = "Unze",
  schank = "Schank",
  scrupel = "Skrupel",
  carat = "Karat",
}

export class Amount {
  private flatAmount?: number;
  private diceCount?: number;
  private diceType?: DiceType;
  private unit: Unit;
  private specificationStr?: string;

  constructor(
    flatAmount?: number,
    diceCount?: number,
    diceType?: DiceType,
    unit: Unit = Unit.count,
    specificationStr?: string
  ) {
    this.flatAmount = flatAmount;
    this.diceCount = diceCount;
    this.diceType = diceType;
    this.unit = unit;
    this.specificationStr = specificationStr;
  }

  get average(): number {
    let amount = 0;
    if (this.flatAmount) amount += this.flatAmount;
    if (this.diceCount && this.diceType)
      amount += this.diceCount * this.diceType.average;
    return amount;
  }
  get str(): string {
    if (!this.flatAmount && !this.diceCount && !this.diceType) {
      return "-";
    }
    let str = "";
    if (this.diceCount && this.diceType)
      str += `${this.diceCount}${this.diceType.str}`;
    if (this.diceCount && this.diceType && this.flatAmount) str += "+";
    if (this.flatAmount) str += `${this.flatAmount}`;
    if (this.unit !== Unit.count) str += ` ${this.unit}`;
    if (this.specificationStr) str += ` ${this.specificationStr}`;
    return str;
  }
}
