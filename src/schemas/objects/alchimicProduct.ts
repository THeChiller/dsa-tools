import type { Amount, Reference } from "@/schemas";
import { Duration, Modificator, ObjectType, Price, TimeFrame } from "@/schemas";
import { getObjectsStore } from "@/stores";

export enum AlchimicProductType {
  utils = "Hilfsmittel",
  crop = "Nutzpflanze",
  ingredient = "alchimistische Zutat",
  heal = "Heilmittel",
  buff = "Stärkungmittel",
  drug = "Rauschmittel",
  poison = "Gift",
  weaponPoison = "Waffengift",
  contactPoison = "Kontaktgift",
  breathingPoison = "Atemgift",
  cosmetics = "Kosmetik",
  spice = "Gewürz",
  repellent = "Insektenschutzmittel",
  painkiller = "Schmerzmittel",
  glue = "Klebstoff",
  preservative = "Konservierungsmittel",
  illuminant = "Leuchtmittel",
  birthControl = "empfängnisverhütendes Mittel",
  smokingAgent = "Räuchermittel",
}

export interface AlchimicIngredient {
  name: string;
  price: Price;
  durability: Duration;
  references: Reference[];
  objectType: ObjectType;
}

export class AlchimicRecipeIngredient {
  amount: Amount;
  private ingredientString: string;

  constructor(amount: Amount, ingredientString: string) {
    this.amount = amount;
    this.ingredientString = ingredientString;
  }

  get ingredient(): AlchimicIngredient {
    const objectStore = getObjectsStore();
    if (objectStore.plantParts[this.ingredientString])
      return objectStore.plantParts[this.ingredientString];
    if (objectStore.alchimicProducts[this.ingredientString])
      return objectStore.alchimicProducts[this.ingredientString];
    if (objectStore.equipment[this.ingredientString])
      return objectStore.equipment[this.ingredientString];
    return {
      name: `not Found (${this.ingredientString})`,
      price: new Price(),
      durability: new Duration(TimeFrame.years),
      references: [],
      objectType: ObjectType.notFound,
    };
  }
}

export type AlchimicModificators = {
  brewing?: Modificator;
  analysis?: Modificator;
};

export class Aquisition {
  minPrice: Price;
  maxPrice: Price;
  rarity: Modificator;

  constructor(minPrice: Price, maxPrice: Price, rarity: Modificator) {
    this.minPrice = minPrice;
    this.maxPrice = maxPrice;
    if (rarity.value < 1) this.rarity = Modificator["1"];
    else this.rarity = rarity;
  }

  get str(): string {
    if (this.minPrice.str === this.maxPrice.str)
      return `${this.minPrice.str} / ${this.rarity.value}`;
    return `${this.minPrice.str} - ${this.maxPrice.str} / ${this.rarity.value}`;
  }
}

export class AlchimicProduct implements AlchimicIngredient {
  name: string;
  types: AlchimicProductType[];
  aquisition: Aquisition;
  recipeIngredients: AlchimicRecipeIngredient[];
  recipeDescription: string;
  laboratory: "Alchimistenlabor" | "Hexenküche" | "Archaisches Labor" | "keins";
  creating: AlchimicModificators;
  effect: string;
  spread: Modificator;
  characteristics: string;
  durability: Duration;
  price: Price;
  gmAdvice: string;
  references: Reference[];
  usedFor: string[];
  poisonStart?: Duration;
  poisonLevel?: number;
  objectType: ObjectType;

  constructor(
    name: string,
    types: AlchimicProductType[],
    aquisition: Aquisition,
    recipeIngredients: AlchimicRecipeIngredient[],
    recipeDescription: string,
    laboratory:
      | "Alchimistenlabor"
      | "Hexenküche"
      | "Archaisches Labor"
      | "keins",
    creating: AlchimicModificators,
    effect: string,
    spread: Modificator,
    characteristics: string,
    durability: Duration,
    price: Price,
    gmAdvice: string,
    references: Reference[],
    usedFor: string[],
    poisonStart?: Duration,
    poisonLevel?: number
  ) {
    this.name = name;
    this.types = types;
    this.aquisition = aquisition;
    this.recipeIngredients = recipeIngredients;
    this.recipeDescription = recipeDescription;
    this.laboratory = laboratory;
    this.creating = creating;
    this.effect = effect;
    if (spread.value < 1) this.spread = Modificator["1"];
    else if (spread.value > 1) this.spread = Modificator["7"];
    else this.spread = spread;
    this.characteristics = characteristics;
    this.durability = durability;
    this.price = price;
    this.gmAdvice = gmAdvice;
    this.references = references;
    this.usedFor = usedFor;
    this.poisonStart = poisonStart;
    this.poisonLevel = poisonLevel;
    this.objectType = ObjectType.alchimicProduct;
  }

  get alchimicProducts(): AlchimicProduct[] {
    const products: AlchimicProduct[] = [];
    const objectStore = getObjectsStore();
    this.usedFor.forEach((name: string) => {
      if (objectStore.alchimicProducts[name])
        products.push(objectStore.alchimicProducts[name]);
    });
    return products;
  }
}
