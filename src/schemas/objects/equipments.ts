import type { AlchimicIngredient, Price, Reference, Weight } from "@/schemas";
import { Duration, ObjectType, TimeFrame } from "@/schemas";

export class Equipment implements AlchimicIngredient {
  name: string;
  price: Price;
  weight: Weight;
  references: Reference[];
  durability: Duration;
  objectType: ObjectType;

  constructor(
    name: string,
    price: Price,
    weight: Weight,
    references: Reference[],
    durability?: Duration,
    objectType?: ObjectType
  ) {
    this.name = name;
    this.price = price;
    this.weight = weight;
    this.references = references;
    if (durability) this.durability = durability;
    else this.durability = new Duration(TimeFrame.years);
    if (objectType) this.objectType = objectType;
    else this.objectType = ObjectType.equipment;
  }
}
