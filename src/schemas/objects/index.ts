export * from "./alchimicProduct";
export * from "./animal";
export * from "./plantPart";
export * from "./plant";
export * from "./equipments";

export enum ObjectType {
  animal = "Tier",
  animalPart = "Tier Teil",
  plant = "Pflanze",
  plantPart = "Pflanzen Teil",
  alchimicProduct = "alchimistisches Produkt",
  alchimicIngredient = "alchimistische Zutat",
  equipment = "Ausrüstung",
  notFound = "not found",
}
