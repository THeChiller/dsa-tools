import type {
  Price,
  Duration,
  AlchimicProduct,
  Plant,
  AlchimicIngredient,
  Reference,
} from "@/schemas";
import { getObjectsStore } from "@/stores";
import { ObjectType } from "@/schemas";

export class PlantPart implements AlchimicIngredient {
  name: string;
  durability: Duration;
  durabilitySpecification: string;
  price: Price;
  amountPerPrice: string;
  doseAndEffect: string;
  alchimicUse: string;
  specialNotes: string;
  private sourcePlants: string[];
  private usedFor: string[];
  references: Reference[];
  poisonStart?: Duration;
  poisonLevel?: number;
  objectType: ObjectType;

  constructor(
    name: string,
    durability: Duration,
    durabilitySpecification: string,
    price: Price,
    amountPerPrice: string,
    doseAndEffect: string,
    alchimicUse: string,
    specialNotes: string,
    sourcePlants: string[],
    usedFor: string[],
    references: Reference[],
    poisonStart?: Duration,
    poisonLevel?: number
  ) {
    this.name = name;
    this.durability = durability;
    this.price = price;
    this.amountPerPrice = amountPerPrice;
    this.durabilitySpecification = durabilitySpecification;
    this.doseAndEffect = doseAndEffect;
    this.alchimicUse = alchimicUse;
    this.specialNotes = specialNotes;
    this.sourcePlants = sourcePlants;
    this.usedFor = usedFor;
    this.references = references;
    this.poisonStart = poisonStart;
    this.poisonLevel = poisonLevel;
    this.objectType = ObjectType.plantPart;
  }

  get priceStr(): string {
    if (this.amountPerPrice !== "")
      return `${this.price.str} ${this.amountPerPrice}`;
    return this.price.str;
  }
  get durabilityStr(): string {
    if (this.durabilitySpecification !== "")
      return `${this.durability.str} (${this.durabilitySpecification})`;
    return this.durability.str;
  }

  get plants(): Plant[] {
    const plants: Plant[] = [];
    const objectStore = getObjectsStore();
    this.sourcePlants.forEach((name: string) => {
      if (objectStore.plants[name]) plants.push(objectStore.plants[name]);
    });
    return plants;
  }

  get alchimicProducts(): AlchimicProduct[] {
    const products: AlchimicProduct[] = [];
    const objectStore = getObjectsStore();
    this.usedFor.forEach((name: string) => {
      if (objectStore.alchimicProducts[name])
        products.push(objectStore.alchimicProducts[name]);
    });
    return products;
  }
}
