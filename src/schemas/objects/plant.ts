import type {
  AventuricArea,
  Spread,
  Reference,
  Modificator,
  PlantPart,
} from "@/schemas";
import type { Moonphase, AventuricMonth, Daytime, Amount } from "@/schemas";

export enum PlantType {
  supernatural = "Übernatuerliche Pflanze",
  heal = "Heilpflanze",
  poison = "Giftpflanze",
  crop = "Nutzpflanze",
  dangerous = "Gefährliche Pflanze",
}

export type HarvestAmount = {
  amount: Amount;
  plantPart: PlantPart;
  factor?: Amount;
};

export class Plant {
  active: boolean;
  name: string;
  types: PlantType[];
  aventuricAreas: AventuricArea[];
  genericSpread: Spread[];
  genericIdentificationModifier: Modificator;
  genericHarvestAmount: HarvestAmount[];
  harvestTime: AventuricMonth[];
  references: Reference[];
  customIdentificationModifier?: (
    month?: AventuricMonth,
    daytime?: Daytime,
    moonPhase?: Moonphase,
    spreads?: Spread[],
    zfpStar?: number
  ) => Modificator;
  customHarvestAmount?: (
    month?: AventuricMonth,
    tapStar?: number,
    moonPhase?: Moonphase,
    daytime?: Daytime,
    luckRoll?: number
  ) => HarvestAmount[];
  customSpread?: (month?: AventuricMonth) => Spread[];

  constructor(
    active = true,
    name: string,
    context: string,
    description: string,
    types: PlantType[],
    aventuricAreas: AventuricArea[],
    genericSpread: Spread[],
    genericIdentificationModifier: Modificator,
    genericHarvestAmount: HarvestAmount[],
    harvestTime: AventuricMonth[],
    references: Reference[],
    customIdentificationModifier?: (
      month?: AventuricMonth,
      daytime?: Daytime,
      moonPhase?: Moonphase,
      spreads?: Spread[],
      zfpStar?: number
    ) => Modificator,
    customHarvestAmount?: (
      month?: AventuricMonth,
      tapStar?: number,
      moonPhase?: Moonphase,
      daytime?: Daytime,
      luckRoll?: number
    ) => Array<HarvestAmount>,
    customSpread?: (month?: AventuricMonth) => Spread[]
  ) {
    this.active = active;
    this.name = name;
    this.types = types;
    this.aventuricAreas = aventuricAreas;
    this.genericSpread = genericSpread;
    this.genericIdentificationModifier = genericIdentificationModifier;
    this.genericHarvestAmount = genericHarvestAmount;
    this.harvestTime = harvestTime;
    this.references = references;
    this.customIdentificationModifier = customIdentificationModifier;
    this.customHarvestAmount = customHarvestAmount;
    this.customSpread = customSpread;
  }

  identificationModifier(
    month?: AventuricMonth,
    daytime?: Daytime,
    moonPhase?: Moonphase,
    spreads?: Spread[],
    zfpStar?: number
  ): Modificator {
    if (this.customIdentificationModifier)
      return this.customIdentificationModifier(
        month,
        daytime,
        moonPhase,
        spreads,
        zfpStar
      );
    return this.genericIdentificationModifier;
  }

  harvestAmount(
    month?: AventuricMonth,
    tapStar?: number,
    moonPhase?: Moonphase,
    daytime?: Daytime,
    luckRoll?: number
  ): HarvestAmount[] {
    if (this.customHarvestAmount)
      return this.customHarvestAmount(
        month,
        tapStar,
        moonPhase,
        daytime,
        luckRoll
      );
    return this.genericHarvestAmount;
  }

  spread(month?: AventuricMonth): Spread[] {
    if (this.customSpread) return this.customSpread(month);
    return this.genericSpread;
  }
}
