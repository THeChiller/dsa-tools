import type { SelectMixedOption } from "naive-ui/lib/select/src/interface";
import { Amount, DiceType } from "@/schemas/amount";

export class TimeFrame {
  public static readonly kr = new TimeFrame(
    "Kampfrunde",
    "Kampfrunden",
    1 / 1200,
    "kr"
  );
  public static readonly sr = new TimeFrame(
    "Spielrunde",
    "Spielrunden",
    1 / 12,
    "sr"
  );
  public static readonly hours = new TimeFrame("Stunde", "Stunden", 1, "hours");
  public static readonly days = new TimeFrame("Tag", "Tage", 24, "days");
  public static readonly weeks = new TimeFrame("Woche", "Wochen", 168, "weeks");
  public static readonly months = new TimeFrame(
    "Monat",
    "Monate",
    720,
    "months"
  );
  public static readonly years = new TimeFrame("Jahr", "Jahre", 8760, "years");

  private constructor(
    public readonly longName: string,
    public readonly plural: string,
    public readonly factor: number,
    public readonly key: string
  ) {}
}

export function getTimeFrameObjectByName(name: string): TimeFrame | undefined {
  const obj: Record<string, TimeFrame> = {};
  for (const TimeFrameKey in TimeFrame) {
    // @ts-ignore:next-line
    obj[TimeFrameKey] = TimeFrame[TimeFrameKey];
  }
  return obj[name];
}

export function getTimeFrameByValue(facor: number): TimeFrame {
  if (facor === TimeFrame.hours.factor) return TimeFrame.hours;
  if (facor === TimeFrame.days.factor) return TimeFrame.days;
  if (facor === TimeFrame.weeks.factor) return TimeFrame.weeks;
  if (facor === TimeFrame.months.factor) return TimeFrame.months;
  return TimeFrame.years;
}

export function getTimeFrameNSelectOptions(
  lessThan?: number,
  greaterThan?: number
): SelectMixedOption[] {
  const array: SelectMixedOption[] = [];
  Object.keys(TimeFrame).forEach((TimeFrameName) => {
    const timeFrame = getTimeFrameObjectByName(TimeFrameName);
    if (!timeFrame) return;
    if (!lessThan && !greaterThan) {
      array.push({
        label: timeFrame.plural,
        value: timeFrame.factor,
      });
    } else if (!lessThan && greaterThan && timeFrame.factor >= greaterThan) {
      array.push({
        label: timeFrame.plural,
        value: timeFrame.factor,
      });
    } else if (lessThan && !greaterThan && timeFrame.factor <= lessThan) {
      array.push({
        label: timeFrame.plural,
        value: timeFrame.factor,
      });
    } else if (
      greaterThan &&
      timeFrame.factor >= greaterThan &&
      lessThan &&
      timeFrame.factor <= lessThan
    ) {
      array.push({
        label: timeFrame.plural,
        value: timeFrame.factor,
      });
    }
  });
  return array;
}

export const minTimeFrame = TimeFrame.hours;
export const maxTimeFrame = TimeFrame.months;

export enum Daytime {
  any = "Egal",
  night = "Nacht",
}

export enum Moonphase {
  any = "Egal",
  fullMoon = "Vollmond",
}

export enum AventuricMonth {
  firun = "Firun",
  tsa = "Tsa",
  phex = "Phex",
  peraine = "Peraine",
  ingerimm = "Ingerimm",
  rahja = "Rahja",
  namelessDays = "Namenlose Tage",
  praios = "Praios",
  rondra = "Rondra",
  efferd = "Efferd",
  travia = "Travia",
  boron = "Boron",
  hesinde = "Hesinde",
}

export const plant_harvest_timeframe_spring: AventuricMonth[] = [
  AventuricMonth.phex,
  AventuricMonth.peraine,
  AventuricMonth.ingerimm,
];
export const plant_harvest_timeframe_summer: AventuricMonth[] = [
  AventuricMonth.rahja,
  AventuricMonth.namelessDays,
  AventuricMonth.praios,
  AventuricMonth.rondra,
];
export const plant_harvest_timeframe_autumn: AventuricMonth[] = [
  AventuricMonth.efferd,
  AventuricMonth.travia,
  AventuricMonth.boron,
];
export const plant_harvest_timeframe_winter: AventuricMonth[] = [
  AventuricMonth.hesinde,
  AventuricMonth.firun,
  AventuricMonth.tsa,
];
export const plant_harvest_timeframe_all: AventuricMonth[] = [
  AventuricMonth.firun,
  AventuricMonth.tsa,
  AventuricMonth.phex,
  AventuricMonth.peraine,
  AventuricMonth.ingerimm,
  AventuricMonth.rahja,
  AventuricMonth.namelessDays,
  AventuricMonth.praios,
  AventuricMonth.rondra,
  AventuricMonth.efferd,
  AventuricMonth.travia,
  AventuricMonth.boron,
  AventuricMonth.hesinde,
];

export class Duration {
  private amount: Amount;
  private timeFrame: TimeFrame;

  constructor(
    timeFrame: TimeFrame,
    flatAmount?: number,
    diceCount?: number,
    diceType?: DiceType
  ) {
    this.amount = new Amount(flatAmount, diceCount, diceType);
    this.timeFrame = timeFrame;
  }

  get str(): string {
    if (this.amount.str === "-") {
      return "-";
    }
    return `${this.amount.str} ${
      this.comparableValue() > 1
        ? this.timeFrame?.plural
        : this.timeFrame?.longName
    }`;
  }
  comparableValue(): number {
    if (this.amount.str === "-") {
      return 1000000000;
    }
    return this.amount.average * this.timeFrame.factor;
  }
}
