export enum Book {
  zba3 = "Zoo-Botanica Aventurica (3. Auflage)",
  srd = "Stäbe, Ringe, Dschinnenlampen",
  ga = "Geographia Aventurica",
  wds = "Wege des Schwerts",
  wdh = "Wege des Helden",
  wda = "Wege der Alchimie",
  wdz = "Wege der Zauberei",
  wdg = "Wege der Göttter",
}
