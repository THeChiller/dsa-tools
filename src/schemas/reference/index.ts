import type { Book } from "@/schemas";
export * from "./book";

export class Reference {
  book: Book;
  pages?: number[];

  constructor(book: Book, pages?: number[]) {
    this.book = book;
    this.pages = pages;
  }

  get str(): string {
    let str = `${this.book}`;
    if (this.pages) {
      str = str + " (";
      this.pages.forEach((page) => {
        str = str + `${page}, `;
      });
      str = str.slice(0, -2) + ")";
    }
    return str;
  }
}
