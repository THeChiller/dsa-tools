import { Reference, Book } from "@/schemas";

export class SpecialAbility {
  public static readonly suitingTerrainKnowledge = new SpecialAbility(
    "passende Geländekunde",
    [new Reference(Book.wdh, [276])]
  );
  public static readonly suitingLocalKnowledge = new SpecialAbility(
    "passende Ortskenntnis",
    [new Reference(Book.wdh, [276])]
  );
  public static readonly sharpshooter = new SpecialAbility("Scharfschütze", [
    new Reference(Book.wdh, [281]),
  ]);
  public static readonly mastershooter = new SpecialAbility("Meisterschütze", [
    new Reference(Book.wdh, [280]),
  ]);
  public static readonly combatReflexes = new SpecialAbility("Kampfreflexe", [
    new Reference(Book.wdh, [279]),
  ]);
  public static readonly combatSense = new SpecialAbility("Kampfgespür", [
    new Reference(Book.wdh, [279]),
  ]);
  public static readonly awareness = new SpecialAbility("Aufmerksamkeit", [
    new Reference(Book.wdh, [277]),
  ]);
  public static readonly iron = new SpecialAbility("Eisern", [
    new Reference(Book.wdh, [250]),
  ]);
  public static readonly glassBones = new SpecialAbility("Glasknochen", [
    new Reference(Book.wdh, [264]),
  ]);
  public static readonly toughDog = new SpecialAbility("Zäher Hund", [
    new Reference(Book.wdh, [259]),
  ]);
  public static readonly bladeDancer = new SpecialAbility("Klingentänzer", [
    new Reference(Book.wdh, [279]),
  ]);
  public static readonly dawnVision = new SpecialAbility("Dämmerungssicht", [
    new Reference(Book.wdh, [249]),
  ]);
  public static readonly nightVision = new SpecialAbility("Nachtsicht", [
    new Reference(Book.wdh, [255]),
  ]);
  public static readonly claustrophobia = new SpecialAbility("Raumangst", [
    new Reference(Book.wdh, [268]),
  ]);
  public static readonly badSensorySight = new SpecialAbility(
    "Eingeschränkter Sinn (Sicht)",
    [new Reference(Book.wdh, [262])]
  );
  public static readonly badSensoryHearing = new SpecialAbility(
    "Eingeschränkter Sinn (Gehör)",
    [new Reference(Book.wdh, [262])]
  );
  public static readonly goodSensorySight = new SpecialAbility(
    "Außerordentlicher Sinn (Sicht)",
    [new Reference(Book.wdh, [262])]
  );
  public static readonly goodSensoryHearing = new SpecialAbility(
    "Außerordentlicher Sinn (Gehör)",
    [new Reference(Book.wdh, [262])]
  );
  public static readonly oneEyed = new SpecialAbility("Einäugig", [
    new Reference(Book.wdh, [262]),
  ]);
  public static readonly imaginations = new SpecialAbility("Einbildungen", [
    new Reference(Book.wdh, [262]),
  ]);
  public static readonly erratic = new SpecialAbility("Unstet", [
    new Reference(Book.wdh, [262]),
  ]);
  public static readonly darkAnxiety = new SpecialAbility("Dunkelangst", [
    new Reference(Book.wdh, [262]),
  ]);
  public static readonly nightBlind = new SpecialAbility("Nachtblind", [
    new Reference(Book.wdh, [262]),
  ]);

  private constructor(
    public readonly name: string,
    public readonly references: Array<Reference>
  ) {}
}
