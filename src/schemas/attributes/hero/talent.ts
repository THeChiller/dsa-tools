export enum TalentType {
  fight = "Kampf",
  physical = "Körperlich",
  society = "Gesellschaft",
  nature = "Natur",
  knowledge = "Wissen",
  language = "Sprachen",
  crafting = "Handwerk",
  gift = "Gaben",
  ritualSkill = "Ritualfertigkeit",
}

export class Talent {
  public static readonly anyRangedWeapon = new Talent(
    "Fernkampfwaffe",
    TalentType.fight
  );
  public static readonly sneak = new Talent("Schleichen", TalentType.physical);
  public static readonly hide = new Talent("Verstecken", TalentType.physical);
  public static readonly sensory = new Talent(
    "Sinnesschärfe",
    TalentType.physical
  );
  public static readonly tracking = new Talent(
    "Fährtensuchen",
    TalentType.nature
  );
  public static readonly trapping = new Talent(
    "Fallen Stellen",
    TalentType.nature
  );
  public static readonly fishing = new Talent(
    "Fischen / Angeln",
    TalentType.nature
  );
  public static readonly wildlife = new Talent(
    "Wildnisleben",
    TalentType.nature
  );
  public static readonly floraKnowledge = new Talent(
    "Pflanzenkunde",
    TalentType.knowledge
  );
  public static readonly faunaKnowledge = new Talent(
    "Tierkunde",
    TalentType.knowledge
  );
  public static readonly selfControl = new Talent(
    "Selbstbeherrschung",
    TalentType.physical
  );
  public static readonly dankerSense = new Talent(
    "Gefahreninstinkt",
    TalentType.gift
  );
  public static readonly alleyKnowledge = new Talent(
    "Gassenwissen",
    TalentType.society
  );

  private constructor(
    public readonly name: string,
    public readonly type: TalentType
  ) {}
}
