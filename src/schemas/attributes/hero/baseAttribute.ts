export class BaseAttribute {
  public static readonly mu = new BaseAttribute("MU", "Mut");
  public static readonly kl = new BaseAttribute("KL", "Klugheit");
  public static readonly in = new BaseAttribute("IN", "Intuition");
  public static readonly ch = new BaseAttribute("CH", "Charisma");
  public static readonly ff = new BaseAttribute("FF", "Fingerfertigkeit");
  public static readonly ge = new BaseAttribute("GE", "Gewandtheit");
  public static readonly ko = new BaseAttribute("KO", "Konstitution");
  public static readonly kk = new BaseAttribute("KK", "Körperkraft");

  private constructor(
    public readonly shortName: string,
    public readonly longName: string
  ) {}
}
