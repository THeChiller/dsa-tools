export class WeightUnit {
  public static readonly u = new WeightUnit("U", "Unze", "Unzen");
  public static readonly s = new WeightUnit("S", "Stein", "Stein");

  private constructor(
    public readonly shortName: string,
    public readonly longName: string,
    public readonly plural: string
  ) {}
}

export class Weight {
  s?: number;
  u?: number;

  constructor(s?: number, u?: number) {
    this.s = s;
    this.u = u;
  }

  get str(): string {
    if (!(this.s && this.u)) {
      return "0S";
    }
    let str = "";
    if (this.s) {
      str = str + `${this.s}${WeightUnit.u.shortName} `;
    }
    if (this.u) {
      str = str + `${this.u}${WeightUnit.u.shortName} `;
    }
    return str.slice(0, -1);
  }

  value(): number {
    if (!(this.s && this.u)) {
      return 0;
    }
    return this.s * 40 + this.u;
  }
}
