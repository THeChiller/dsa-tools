export class Modificator {
  public static readonly "-28" = new Modificator(-28);
  public static readonly "-27" = new Modificator(-27);
  public static readonly "-26" = new Modificator(-26);
  public static readonly "-25" = new Modificator(-25);
  public static readonly "-24" = new Modificator(-24);
  public static readonly "-23" = new Modificator(-23);
  public static readonly "-22" = new Modificator(-22);
  public static readonly "-21" = new Modificator(-21);
  public static readonly "-20" = new Modificator(-20);
  public static readonly "-19" = new Modificator(-19);
  public static readonly "-18" = new Modificator(-18);
  public static readonly "-17" = new Modificator(-17);
  public static readonly "-16" = new Modificator(-16);
  public static readonly "-15" = new Modificator(-15);
  public static readonly "-14" = new Modificator(-14);
  public static readonly "-13" = new Modificator(-13);
  public static readonly "-12" = new Modificator(-12);
  public static readonly "-11" = new Modificator(-11);
  public static readonly "-10" = new Modificator(-10);
  public static readonly "-9" = new Modificator(-9);
  public static readonly "-8" = new Modificator(-8);
  public static readonly "-7" = new Modificator(-7);
  public static readonly "-6" = new Modificator(-6);
  public static readonly "-5" = new Modificator(-5);
  public static readonly "-4" = new Modificator(-4);
  public static readonly "-3" = new Modificator(-3);
  public static readonly "-2" = new Modificator(-2);
  public static readonly "-1" = new Modificator(-1);
  public static readonly "0" = new Modificator(0);
  public static readonly "1" = new Modificator(1);
  public static readonly "2" = new Modificator(2);
  public static readonly "3" = new Modificator(3);
  public static readonly "4" = new Modificator(4);
  public static readonly "5" = new Modificator(5);
  public static readonly "6" = new Modificator(6);
  public static readonly "7" = new Modificator(7);
  public static readonly "8" = new Modificator(8);
  public static readonly "9" = new Modificator(9);
  public static readonly "10" = new Modificator(10);
  public static readonly "11" = new Modificator(11);
  public static readonly "12" = new Modificator(12);
  public static readonly "13" = new Modificator(13);
  public static readonly "14" = new Modificator(14);
  public static readonly "15" = new Modificator(15);
  public static readonly "16" = new Modificator(16);
  public static readonly "17" = new Modificator(17);
  public static readonly "18" = new Modificator(18);
  public static readonly "19" = new Modificator(19);
  public static readonly "20" = new Modificator(20);
  public static readonly "21" = new Modificator(21);
  public static readonly "22" = new Modificator(22);
  public static readonly "23" = new Modificator(23);
  public static readonly "24" = new Modificator(24);
  public static readonly "25" = new Modificator(25);
  public static readonly "26" = new Modificator(26);
  public static readonly "27" = new Modificator(27);
  public static readonly "28" = new Modificator(28);

  private constructor(public readonly value: number) {}
}

export function getModificatorObject() {
  const obj: Record<string, number> = {};
  for (const IdentificationKey in Modificator) {
    // @ts-ignore:next-line
    obj[Modificator[IdentificationKey].displayName()] =
      // @ts-ignore:next-line
      Modificator[IdentificationKey].value;
  }
  return obj;
}

export function getModificatorObjectByValue(value: number): Modificator {
  const obj: Record<string, Modificator> = {};
  for (const IdentificationKey in Modificator) {
    // @ts-ignore:next-line
    obj[IdentificationKey] =
      // @ts-ignore:next-line
      Modificator[IdentificationKey];
  }
  return obj[value];
}
