import { GeographicArea, Rarity } from "@/schemas";
import type { SelectMixedOption } from "naive-ui/lib/select/src/interface";

export class Spread {
  geographicArea: GeographicArea;
  rarity: Rarity;

  constructor(geographicArea: GeographicArea, rarity: Rarity) {
    this.geographicArea = geographicArea;
    this.rarity = rarity;
  }

  str() {
    return `${this.geographicArea} - ${this.rarity.displayName()}`;
  }
}

export function getAllSpreads(): Spread[] {
  const array: Spread[] = [];
  for (const geographicAreaKey in GeographicArea) {
    for (const rarityKey in Rarity) {
      array.push(
        // @ts-ignore:next-line
        new Spread(GeographicArea[geographicAreaKey], Rarity[rarityKey])
      );
    }
  }
  return array;
}

export function getAllSpreadsExcept(
  areas: GeographicArea[],
  rarity: Rarity
): Spread[] {
  const array: Spread[] = [];
  for (const geographicAreaKey in GeographicArea) {
    // @ts-ignore:next-line
    if (areas.includes(GeographicArea[geographicAreaKey])) continue;
    array.push(
      // @ts-ignore:next-line
      new Spread(GeographicArea[geographicAreaKey], rarity)
    );
  }
  return array;
}

export function getSpreadNSelectOptions(): SelectMixedOption[] {
  const array: SelectMixedOption[] = [];
  getAllSpreads().forEach((spread) => {
    array.push({
      label: spread.str(),
      value: spread.str(),
    });
  });
  return array;
}
