import type { SelectMixedOption } from "naive-ui/lib/select/src/interface";

export class Currency {
  public static readonly d = new Currency("D", "Dukat", "Dukaten", 1000);
  public static readonly s = new Currency(
    "S",
    "Silbertaler",
    "Silbertaler",
    100
  );
  public static readonly h = new Currency("H", "Heller", "Heller", 10);
  public static readonly k = new Currency("K", "Kreuzer", "Kreuzer", 1);

  private constructor(
    public readonly shortName: string,
    public readonly longName: string,
    public readonly plural: string,
    public readonly factor: number
  ) {}
}

export const minCurrency = Currency.k;
export const maxCurrency = Currency.d;

export function getCurrencyObjectByName(name: string): Currency | undefined {
  const obj: Record<string, Currency> = {};
  for (const CurrencyKey in Currency) {
    // @ts-ignore:next-line
    obj[CurrencyKey] = Currency[CurrencyKey];
  }
  return obj[name];
}

export function getCurrencyNSelectOptions(
  lessThan?: number,
  greaterThan?: number
): SelectMixedOption[] {
  const array: SelectMixedOption[] = [];
  Object.keys(Currency).forEach((CurrencyName) => {
    const currency = getCurrencyObjectByName(CurrencyName);
    if (!currency) return;
    if (!lessThan && !greaterThan) {
      array.push({
        label: currency.longName,
        value: currency.factor,
      });
    } else if (!lessThan && greaterThan && currency.factor >= greaterThan) {
      array.push({
        label: currency.longName,
        value: currency.factor,
      });
    } else if (lessThan && !greaterThan && currency.factor <= lessThan) {
      array.push({
        label: currency.longName,
        value: currency.factor,
      });
    } else if (
      greaterThan &&
      currency.factor >= greaterThan &&
      lessThan &&
      currency.factor <= lessThan
    ) {
      array.push({
        label: currency.longName,
        value: currency.factor,
      });
    }
  });
  return array;
}

export class Price {
  d?: number;
  s?: number;
  h?: number;
  k?: number;
  none?: boolean;

  constructor(d?: number, s?: number, h?: number, k?: number, none?: boolean) {
    this.d = d;
    this.s = s;
    this.h = h;
    this.k = k;
    this.none = none;
  }

  get str(): string {
    if (this.none) {
      return "0";
    }
    if (!this.d && !this.s && !this.h && !this.k) {
      return "unverkäuflich";
    }
    let str = "";
    if (this.d) {
      str = str + `${this.d}${Currency.d.shortName} `;
    }
    if (this.s) {
      str = str + `${this.s}${Currency.s.shortName} `;
    }
    if (this.h) {
      str = str + `${this.h}${Currency.h.shortName} `;
    }
    if (this.k) {
      str = str + `${this.k}${Currency.k.shortName} `;
    }
    return str.slice(0, -1);
  }

  get comparableValue(): number {
    if (!this.d && !this.s && !this.h && !this.k) {
      return -1;
    }
    if (this.none) return 0;
    return (
      (this.d ? this.d : 0) * Currency.d.factor +
      (this.s ? this.s : 0) * Currency.s.factor +
      (this.h ? this.h : 0) * Currency.h.factor +
      (this.k ? this.k : 0) * Currency.k.factor
    );
  }
}
