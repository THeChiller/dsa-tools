import type { SelectMixedOption } from "naive-ui/lib/select/src/interface";

export class Rarity {
  public static readonly extremeRare = new Rarity(
    "sehr selten",
    -16,
    "extremeRare"
  );
  public static readonly rare = new Rarity("selten", -8, "rare");
  public static readonly sometimes = new Rarity(
    "gelegentlich",
    -4,
    "sometimes"
  );
  public static readonly common = new Rarity("häufig", -2, "common");
  public static readonly extremeCommon = new Rarity(
    "sehr häufig",
    -1,
    "extremeCommon"
  );

  private constructor(
    public readonly name: string,
    public readonly value: number,
    public readonly key: string
  ) {}

  public displayName = (): string => `${this.name} (${this.value})`;
}

export const smallestRarity = Rarity.extremeRare;
export const biggestRarity = Rarity.extremeCommon;

export function getRarityByValue(value: number): Rarity | undefined {
  if (value === Rarity.extremeRare.value) return Rarity.extremeRare;
  if (value === Rarity.rare.value) return Rarity.rare;
  if (value === Rarity.sometimes.value) return Rarity.sometimes;
  if (value === Rarity.common.value) return Rarity.common;
  if (value === Rarity.extremeCommon.value) return Rarity.extremeCommon;
  return undefined;
}

export function getRarities(min?: number, max?: number): Rarity[] {
  const rarities: Rarity[] = [];
  const Rarity = getRarityObject();
  Object.keys(Rarity).forEach((RarityName) => {
    if (!min && !max) {
      const rarity = getRarityByValue(Rarity[RarityName]);
      if (rarity) rarities.push(rarity);
    } else if (!min && max && Rarity[RarityName] <= max) {
      const rarity = getRarityByValue(Rarity[RarityName]);
      if (rarity) rarities.push(rarity);
    } else if (min && !max && Rarity[RarityName] >= min) {
      const rarity = getRarityByValue(Rarity[RarityName]);
      if (rarity) rarities.push(rarity);
    } else if (
      max &&
      Rarity[RarityName] <= max &&
      min &&
      Rarity[RarityName] >= min
    ) {
      const rarity = getRarityByValue(Rarity[RarityName]);
      if (rarity) rarities.push(rarity);
    }
  });
  return rarities;
}

export function getRarityObject(): Record<string, number> {
  const obj: Record<string, number> = {};
  obj[Rarity.extremeCommon.displayName()] = Rarity.extremeCommon.value;
  obj[Rarity.common.displayName()] = Rarity.common.value;
  obj[Rarity.sometimes.displayName()] = Rarity.sometimes.value;
  obj[Rarity.rare.displayName()] = Rarity.rare.value;
  obj[Rarity.extremeRare.displayName()] = Rarity.extremeRare.value;
  return obj;
}

export function getRarityNSelectOptions(
  lessThan?: number,
  greaterThan?: number
): SelectMixedOption[] {
  const array: SelectMixedOption[] = [];
  const Rarity = getRarityObject();
  Object.keys(Rarity).forEach((RarityName) => {
    if (!lessThan && !greaterThan) {
      array.push({
        label: RarityName,
        value: Rarity[RarityName],
      });
    } else if (!lessThan && greaterThan && Rarity[RarityName] >= greaterThan) {
      array.push({
        label: RarityName,
        value: Rarity[RarityName],
      });
    } else if (lessThan && !greaterThan && Rarity[RarityName] <= lessThan) {
      array.push({
        label: RarityName,
        value: Rarity[RarityName],
      });
    } else if (
      greaterThan &&
      Rarity[RarityName] >= greaterThan &&
      lessThan &&
      Rarity[RarityName] <= lessThan
    ) {
      array.push({
        label: RarityName,
        value: Rarity[RarityName],
      });
    }
  });
  return array;
}
