import type {
  AventuricArea,
  Modificator,
  AventuricMonth,
  PlantType,
  Spread,
  Daytime,
  Moonphase,
} from "@/schemas";

export interface PlantFilterType {
  types: PlantType[];
  aventuricAreas: AventuricArea[];
  spreads: Spread[];
  identificationModifierMin?: Modificator;
  identificationModifierMax?: Modificator;
  harvestTime: AventuricMonth;
  harvestDaytime: Daytime;
  harvestMoonPhase: Moonphase;
  costCalculation: "min" | "avg" | "max";
  masterModifier: number;
  magicModifierZFPStar: number;
}
