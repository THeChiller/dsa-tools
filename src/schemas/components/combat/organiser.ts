import { reactive } from "vue";
import { Action, getActionObjectFromAction } from "@/schemas";
import type { ActionObjectType } from "@/schemas";

export enum pointerStatusColor {
  red = "rgba(200,0,0,0.5)",
  yellow = "rgba(255,200,0,0.5)",
  green = "rgba(90,200,0,0.5)",
}

export enum combatantStatus {
  dead = "tot",
  lifeThreateninglyInjured = "lebensgefährlich verletzt",
  incapacitated = "kampfunfähig",
  alive = "lebendig",
}

type combatPhase = "building" | "announcement" | "combat";

export class Combatant {
  id: null | number;

  name: string;

  currentHP: number;
  startCombatHP: number;
  maxHP: number;
  wounds: number;

  constitution: number;

  baseInitiative: number;
  bonusInitiative: number;
  armorInitiativeMod: number;
  weaponInitiativeMod: number;
  bladeDancerInitiativeMod: number;

  combatReflexes: boolean;
  combatSense: boolean;
  awareness: boolean;
  iron: boolean;
  glassBones: boolean;
  toughDog: boolean;

  action1?: ActionObjectType;
  action2?: ActionObjectType;
  longerAction?: ActionObjectType;

  awarenessSelected: boolean;
  actionInitiative?: number;
  selectedActionNumber?: number;
  selectedAction?: ActionObjectType;

  freeAction1?: string;
  freeAction2?: string;
  defenseAction1: boolean;
  defenseAction2: boolean;

  notes: string;

  constructor(
    name: string,
    currentHP: number,
    maxHP: number,
    constitution: number,
    baseInitiative: number,
    bonusInitiative: number,
    armorInitiativeMod: number,
    weaponInitiativeMod: number,
    bladeDancerInitiativeMod: number,
    combatReflexes: boolean,
    combatSense: boolean,
    awareness: boolean,
    iron: boolean,
    glassBones: boolean,
    toughDog: boolean,
    notes: string
  ) {
    this.name = name;
    this.currentHP = currentHP;
    this.startCombatHP = currentHP;
    this.maxHP = maxHP;
    this.constitution = constitution;
    this.baseInitiative = baseInitiative;
    this.bonusInitiative = bonusInitiative;
    this.armorInitiativeMod = armorInitiativeMod;
    this.weaponInitiativeMod = weaponInitiativeMod;
    this.bladeDancerInitiativeMod = bladeDancerInitiativeMod;
    this.combatReflexes = combatReflexes;
    this.combatSense = combatSense;
    this.awareness = awareness;
    this.iron = iron;
    this.glassBones = glassBones;
    this.toughDog = toughDog;
    this.notes = notes;

    this.id = null;
    this.wounds = 0;
    this.awarenessSelected = false;
    this.freeAction1 = undefined;
    this.freeAction2 = undefined;
    this.defenseAction1 = false;
    this.defenseAction2 = false;
  }
  initiative(): number {
    let initiative =
      this.baseInitiative +
      this.bonusInitiative -
      this.armorInitiativeMod +
      this.weaponInitiativeMod +
      this.bladeDancerInitiativeMod -
      this.wounds * 2;
    if (this.combatReflexes) initiative = initiative + 4;
    if (this.combatSense) initiative = initiative + 2;
    return initiative;
  }

  status(): combatantStatus {
    if (this.currentHP < -1.5 * this.constitution) return combatantStatus.dead;
    if (!this.toughDog && this.currentHP < -1 * this.constitution)
      return combatantStatus.dead;
    if (this.currentHP < 1) return combatantStatus.lifeThreateninglyInjured;
    if (this.currentHP <= 5 && !this.toughDog && !this.iron)
      return combatantStatus.incapacitated;
    return combatantStatus.alive;
  }

  updateHitpoints(change: number, bluntWeapon: boolean): number {
    this.currentHP = this.currentHP + change;
    const oldWoundCount = this.wounds;
    if (change < 0) {
      change = Math.abs(change);
      let woundThresholdBoni = 0;
      if (this.iron) woundThresholdBoni = woundThresholdBoni + 2;
      if (this.glassBones) woundThresholdBoni = woundThresholdBoni - 2;
      if (bluntWeapon) woundThresholdBoni = woundThresholdBoni + 2;

      const halfCO = Math.round(this.constitution / 2);
      if (change > halfCO + woundThresholdBoni) this.wounds++;
      if (change > 2 * halfCO + woundThresholdBoni) this.wounds++;
      if (change > 3 * halfCO + woundThresholdBoni) this.wounds++;
    }
    return this.wounds - oldWoundCount;
  }
  getCopy(): Combatant {
    const copy = reactive(
      new Combatant(
        this.name,
        this.currentHP,
        this.maxHP,
        this.constitution,
        this.baseInitiative,
        this.bonusInitiative,
        this.armorInitiativeMod,
        this.weaponInitiativeMod,
        this.bladeDancerInitiativeMod,
        this.combatReflexes,
        this.combatSense,
        this.awareness,
        this.iron,
        this.glassBones,
        this.toughDog,
        this.notes
      )
    );
    copy.id = this.id;
    copy.startCombatHP = this.startCombatHP;
    copy.wounds = this.wounds;
    copy.awarenessSelected = this.awarenessSelected;
    copy.freeAction1 = this.freeAction1;
    copy.freeAction2 = this.freeAction2;
    copy.defenseAction1 = this.defenseAction1;
    copy.defenseAction2 = this.defenseAction2;
    copy.selectedActionNumber = this.selectedActionNumber;
    copy.selectedAction = this.selectedAction;
    copy.action1 = this.action1;
    copy.action2 = this.action2;
    copy.actionInitiative = this.actionInitiative;
    copy.longerAction = this.longerAction;
    return copy;
  }
}

export class CombatSimulationLog {
  text: string;
  bold: boolean;
  underline: boolean;
  italic: boolean;
  time: Date;
  type: "default" | "success" | "info" | "warning" | "error";

  constructor(
    text: string,
    type?:
      | "damage"
      | "statusChange"
      | "combatantListChange"
      | "roundChange"
      | "announcement"
  ) {
    this.text = text;
    this.bold = false;
    this.underline = false;
    this.italic = false;
    switch (type) {
      case "damage":
        this.type = "warning";
        break;
      case "statusChange":
        this.type = "error";
        this.underline = true;
        break;
      case "roundChange":
        this.type = "info";
        this.underline = true;
        this.bold = true;
        break;
      case "combatantListChange":
        this.type = "info";
        this.italic = true;
        break;
      case "announcement":
        this.type = "success";
        break;
      default:
        this.type = "default";
    }
    this.time = new Date();
  }

  formattedText(): string {
    return `${this.text}`;
  }
  formattedTimestamp(): string {
    return `[${this.time.getFullYear()}/${this.time.getMonth()}/${this.time.getDay()}\xa0${this.time.getHours()}:${this.time.getMinutes()}}]`;
  }
}

export class CombatSimulation {
  combatantID: number;
  combatPhase: combatPhase;
  roundCounter: number;

  combatantsPointer: number;
  combatants: Combatant[];

  combatRoundPointer: number;
  combatRound: Combatant[];

  announcementRoundPointer: number;
  announcementRound: Combatant[];

  logs: CombatSimulationLog[];

  constructor() {
    this.combatantID = 0;
    this.combatPhase = "building";
    this.roundCounter = 0;

    this.combatantsPointer = 0;
    this.combatants = [];

    this.combatRoundPointer = 0;
    this.combatRound = [];

    this.announcementRoundPointer = 0;
    this.announcementRound = [];

    this.logs = [];
  }

  exportToJSON(): string {
    return JSON.stringify(this, null, 4);
  }
  importFromJSON(data: string) {
    const jsonData = JSON.parse(data);

    const combatants: Combatant[] = [];
    const tmpCombatants: Combatant[] = jsonData["combatants"];
    for (const data of tmpCombatants) {
      const tmpCombatant = new Combatant(
        data.name,
        data.currentHP,
        data.maxHP,
        data.constitution,
        data.baseInitiative,
        data.bonusInitiative,
        data.armorInitiativeMod,
        data.weaponInitiativeMod,
        data.bladeDancerInitiativeMod,
        data.combatReflexes,
        data.combatSense,
        data.awareness,
        data.iron,
        data.glassBones,
        data.toughDog,
        data.notes
      );
      tmpCombatant.id = data.id;
      tmpCombatant.startCombatHP = data.startCombatHP;
      tmpCombatant.wounds = data.wounds;
      tmpCombatant.awarenessSelected = data.awarenessSelected;
      tmpCombatant.freeAction1 = data.freeAction1;
      tmpCombatant.freeAction2 = data.freeAction2;
      tmpCombatant.defenseAction1 = data.defenseAction1;
      tmpCombatant.defenseAction2 = data.defenseAction2;
      tmpCombatant.selectedActionNumber = data.selectedActionNumber;
      tmpCombatant.selectedAction = data.selectedAction;
      tmpCombatant.action1 = data.action1;
      tmpCombatant.action2 = data.action2;
      tmpCombatant.actionInitiative = data.actionInitiative;
      tmpCombatant.longerAction = data.longerAction;
      combatants.push(tmpCombatant);
    }
    delete jsonData["combatants"];

    const combatRound: Combatant[] = [];
    const tmpCombatRound: Combatant[] = jsonData["combatRound"];
    for (const data of tmpCombatRound) {
      const tmpCombatant = new Combatant(
        data.name,
        data.currentHP,
        data.maxHP,
        data.constitution,
        data.baseInitiative,
        data.bonusInitiative,
        data.armorInitiativeMod,
        data.weaponInitiativeMod,
        data.bladeDancerInitiativeMod,
        data.combatReflexes,
        data.combatSense,
        data.awareness,
        data.iron,
        data.glassBones,
        data.toughDog,
        data.notes
      );
      tmpCombatant.id = data.id;
      tmpCombatant.startCombatHP = data.startCombatHP;
      tmpCombatant.wounds = data.wounds;
      tmpCombatant.awarenessSelected = data.awarenessSelected;
      tmpCombatant.freeAction1 = data.freeAction1;
      tmpCombatant.freeAction2 = data.freeAction2;
      tmpCombatant.defenseAction1 = data.defenseAction1;
      tmpCombatant.defenseAction2 = data.defenseAction2;
      tmpCombatant.selectedActionNumber = data.selectedActionNumber;
      tmpCombatant.selectedAction = data.selectedAction;
      tmpCombatant.action1 = data.action1;
      tmpCombatant.action2 = data.action2;
      tmpCombatant.actionInitiative = data.actionInitiative;
      tmpCombatant.longerAction = data.longerAction;
      combatRound.push(tmpCombatant);
    }
    delete jsonData["combatRound"];

    const announcementRound: Combatant[] = [];
    const tmpAnnouncementRound: Combatant[] = jsonData["announcementRound"];
    for (const data of tmpAnnouncementRound) {
      const tmpCombatant = new Combatant(
        data.name,
        data.currentHP,
        data.maxHP,
        data.constitution,
        data.baseInitiative,
        data.bonusInitiative,
        data.armorInitiativeMod,
        data.weaponInitiativeMod,
        data.bladeDancerInitiativeMod,
        data.combatReflexes,
        data.combatSense,
        data.awareness,
        data.iron,
        data.glassBones,
        data.toughDog,
        data.notes
      );
      tmpCombatant.id = data.id;
      tmpCombatant.startCombatHP = data.startCombatHP;
      tmpCombatant.wounds = data.wounds;
      tmpCombatant.awarenessSelected = data.awarenessSelected;
      tmpCombatant.freeAction1 = data.freeAction1;
      tmpCombatant.freeAction2 = data.freeAction2;
      tmpCombatant.defenseAction1 = data.defenseAction1;
      tmpCombatant.defenseAction2 = data.defenseAction2;
      tmpCombatant.selectedActionNumber = data.selectedActionNumber;
      tmpCombatant.selectedAction = data.selectedAction;
      tmpCombatant.action1 = data.action1;
      tmpCombatant.action2 = data.action2;
      tmpCombatant.actionInitiative = data.actionInitiative;
      tmpCombatant.longerAction = data.longerAction;
      announcementRound.push(tmpCombatant);
    }
    delete jsonData["announcementRound"];

    const logs: CombatSimulationLog[] = [];
    const tmpLogs: CombatSimulationLog[] = jsonData["logs"];
    for (const data of tmpLogs) {
      const tmpLog = new CombatSimulationLog("");
      tmpLog.text = data.text;
      tmpLog.bold = data.bold;
      tmpLog.underline = data.underline;
      tmpLog.italic = data.italic;
      tmpLog.time = new Date(data.time);
      tmpLog.type = data.type;
      logs.push(tmpLog);
    }
    delete jsonData["logs"];

    Object.assign(this, jsonData);
    this.combatants = combatants;
    this.combatRound = combatRound;
    this.announcementRound = announcementRound;
    this.logs = logs;
  }

  addNewLog(
    text: string,
    type?:
      | "damage"
      | "statusChange"
      | "combatantListChange"
      | "roundChange"
      | "announcement"
  ) {
    this.logs.unshift(reactive(new CombatSimulationLog(text, type)));
  }

  addNewCombatant(combatant: Combatant) {
    combatant.id = this.combatantID;
    this.combatantID++;
    this.combatants.push(reactive(combatant));
    this.combatants.sort(
      (combatantA: Combatant, combatantB: Combatant): number => {
        if (combatantA.initiative() < combatantB.initiative()) return 1;
        if (combatantA.initiative() > combatantB.initiative()) return -1;
        if (combatantA.initiative() === combatantB.initiative()) {
          if (combatantA.baseInitiative < combatantB.baseInitiative) return 1;
          if (combatantA.baseInitiative > combatantB.baseInitiative) return -1;
        }
        return 0;
      }
    );
    this.addNewLog(
      `${combatant.name} tritt dem Kampf bei.`,
      "combatantListChange"
    );
  }
  getCombatantByID(id: number): Combatant | undefined {
    let targetCombatant: Combatant | undefined = undefined;
    this.combatants.forEach((combatant) => {
      if (combatant.id == id) targetCombatant = combatant;
    });
    return targetCombatant;
  }
  removeCombatant(id: number) {
    if (this.pointerAtEndOfList()) {
      if (this.combatPhase === "building")
        this.combatantsPointer = Math.max(0, this.combatantsPointer - 1);
      if (this.combatPhase === "announcement")
        this.announcementRoundPointer = Math.max(
          0,
          this.announcementRoundPointer - 1
        );
      if (this.combatPhase === "combat")
        this.combatRoundPointer = Math.max(0, this.combatRoundPointer - 1);
    }
    this.addNewLog(
      `${this.getCombatantByID(id)?.name} verlässt den Kampf.`,
      "combatantListChange"
    );
    this.combatants.forEach((combatant: Combatant, index: number) => {
      if (combatant.id == id) this.combatants.splice(index, 1);
    });
    this.combatRound.forEach((combatant: Combatant, index: number) => {
      if (combatant.id == id) this.combatRound.splice(index, 1);
    });
    this.announcementRound.forEach((combatant: Combatant, index: number) => {
      if (combatant.id == id) this.announcementRound.splice(index, 1);
    });
  }
  getCurrentCombatant(): Combatant | undefined {
    return this.getCurrentList()[this.getCurrentPointer()];
  }
  getCurrentPointer(): number {
    if (this.combatPhase === "announcement")
      return this.announcementRoundPointer;
    if (this.combatPhase === "combat") return this.combatRoundPointer;
    return this.combatantsPointer;
  }
  getCurrentList(): Combatant[] {
    if (this.combatPhase === "announcement") return this.announcementRound;
    if (this.combatPhase === "combat") return this.combatRound;
    return this.combatants;
  }
  pointerAtEndOfList(): boolean {
    return this.getCurrentPointer() + 1 == this.getCurrentList().length;
  }
  setPointer(target: number) {
    if (this.combatPhase === "announcement")
      this.announcementRoundPointer = target;
    if (this.combatPhase === "combat") this.combatRoundPointer = target;
    this.combatantsPointer = target;
  }
  buildCombatRound(resetCounter = true) {
    this.combatRound = [];
    this.announcementRound.forEach((combatant: Combatant) => {
      const action1 = combatant.getCopy();
      action1.selectedActionNumber = 1;
      action1.selectedAction = combatant.action1;
      action1.actionInitiative = Math.max(
        combatant.initiative() + (combatant.action1?.iniMalus || 0),
        0
      );
      if (action1.selectedAction?.name === Action.defense.name)
        action1.actionInitiative = 0;

      const action2 = combatant.getCopy();
      action2.selectedActionNumber = 2;
      action2.selectedAction = combatant.action2;
      action2.actionInitiative =
        combatant.initiative() + (combatant.action2?.iniMalus || 0);
      if (action2.selectedAction?.name === Action.defense.name)
        action2.actionInitiative = 0;

      if (
        combatant.initiative() > 0 &&
        Number(Boolean(action1.actionInitiative)) >= 0
      )
        this.combatRound.push(action1);
      if (
        combatant.initiative() > 0 &&
        Number(Boolean(action2.actionInitiative)) >= 0
      )
        this.combatRound.push(action2);
    });
    this.combatRound.sort(
      (combatantA: Combatant, combatantB: Combatant): number => {
        if (combatantA.actionInitiative === undefined || combatantB.actionInitiative === undefined)
          return 0;
        if (combatantA.actionInitiative < combatantB.actionInitiative) return 1;
        if (combatantA.actionInitiative > combatantB.actionInitiative) return -1;
        if (combatantA.actionInitiative === combatantB.actionInitiative) {
          if (combatantA.baseInitiative < combatantB.baseInitiative) return 1;
          if (combatantA.baseInitiative > combatantB.baseInitiative) return -1;
        }
        return 0;
      }
    );
    if (resetCounter) this.combatRoundPointer = 0;
    else if (this.combatRoundPointer >= this.combatRound.length)
      this.combatRoundPointer = this.combatRound.length - 1;
  }
  buildAnnouncementRound() {
    this.announcementRound = [];
    this.combatants.forEach((combatant: Combatant) => {
      const tmp = combatant.getCopy();
      if (tmp.longerAction) {
        tmp.action1 = {
          name: tmp.longerAction.name,
          duration: tmp.longerAction.duration,
          iniMalus: tmp.longerAction.iniMalus,
          diceModifier: tmp.longerAction.diceModifier,
          type: tmp.longerAction.type,
        };
        if (tmp.longerAction.duration) tmp.longerAction.duration--;
        if (tmp.action1.duration && tmp.action1.duration > 1) {
          tmp.action2 = {
            name: tmp.action1.name,
            duration: tmp.action1.duration - 1,
            iniMalus: tmp.action1.iniMalus,
            diceModifier: tmp.action1.diceModifier,
            type: tmp.action1.type,
          };
          if (tmp.longerAction.duration) tmp.longerAction.duration--;
          if (tmp.action2.duration && tmp.action2.duration <= 1)
            combatant.longerAction = undefined;
        } else {
          tmp.action2 = getActionObjectFromAction(Action.defense);
          tmp.action2.duration = 1;
          tmp.action2.diceModifier = 0;
          combatant.longerAction = undefined;
        }
      } else {
        tmp.action1 = getActionObjectFromAction(Action.attack);
        tmp.action1.duration = 1;
        tmp.action1.diceModifier = 0;
        tmp.action2 = getActionObjectFromAction(Action.defense);
        tmp.action2.duration = 1;
        tmp.action2.diceModifier = 0;
      }
      this.announcementRound.push(tmp);
    });
    this.announcementRound = this.announcementRound.reverse();
    this.announcementRoundPointer = 0;
  }
  combatStep() {
    if (this.combatPhase === "combat") {
      this.combatRoundPointer++;
      if (this.combatRoundPointer === this.combatRound.length) {
        this.roundCounter++;
        this.addNewLog(`Runde${this.roundCounter} beginnt.`, "roundChange");
        this.buildAnnouncementRound();
        this.combatPhase = "announcement";
      }
    } else if (this.combatPhase === "announcement") {
      this.announcementRoundPointer++;
      if (this.announcementRoundPointer === this.announcementRound.length) {
        this.buildCombatRound();
        this.combatPhase = "combat";
      }
    } else if (this.combatPhase === "building") {
      this.roundCounter = 1;
      this.addNewLog(`Runde${this.roundCounter} beginnt.`, "roundChange");
      this.buildAnnouncementRound();
      this.combatPhase = "announcement";
      return;
    }
  }
  updateCombatantHP(
    id: number,
    change: number,
    bluntWeapon: boolean,
    selectedAction: number
  ) {
    const oldCombatant = this.getCombatantByID(id);
    const oldStatus = oldCombatant?.status();

    this.addNewLog(`${oldCombatant?.name} erhält ${change} SP.`, "damage");
    let receivedWoundCount = 0;
    this.combatants.forEach((combatant: Combatant) => {
      if (combatant.id == id)
        receivedWoundCount = combatant.updateHitpoints(change, bluntWeapon);
    });
    this.announcementRound.forEach((combatant: Combatant) => {
      if (combatant.id == id) combatant.updateHitpoints(change, bluntWeapon);
    });
    this.combatRound.forEach((combatant: Combatant) => {
      if (combatant.id == id) combatant.updateHitpoints(change, bluntWeapon);
    });
    if (receivedWoundCount > 0) {
      this.addNewLog(
        `${
          this.getCombatantByID(id)?.name
        } erhält ${receivedWoundCount} Wunden.`,
        "statusChange"
      );
      this.rebuildCombatRound(id, selectedAction);
    }
    if (oldStatus !== this.getCombatantByID(id)?.status())
      this.addNewLog(
        `${oldCombatant?.name} ist ${this.getCombatantByID(id)?.status()}.`,
        "statusChange"
      );
  }

  rebuildCombatRound(id: number, selectedAction: number) {
    this.buildCombatRound();
    this.combatRound.forEach((combatant, index) => {
      if (
        combatant.id === id &&
        combatant.selectedActionNumber === selectedAction
      )
        this.setPointer(index);
    });
  }

  updateCombatantDefenseAction1(checked: boolean, id: number) {
    this.announcementRound.forEach((combatant: Combatant) => {
      if (combatant.id == id) combatant.defenseAction1 = checked;
    });
    this.combatRound.forEach((combatant: Combatant) => {
      if (combatant.id == id) combatant.defenseAction1 = checked;
    });
  }
  updateCombatantDefenseAction2(checked: boolean, id: number) {
    this.announcementRound.forEach((combatant: Combatant) => {
      if (combatant.id == id) combatant.defenseAction2 = checked;
    });
    this.combatRound.forEach((combatant: Combatant) => {
      if (combatant.id == id) combatant.defenseAction2 = checked;
    });
  }
  updateCombatantFreeAction1(value: string, id: number) {
    this.announcementRound.forEach((combatant: Combatant) => {
      if (combatant.id == id) combatant.freeAction1 = value;
    });
    this.combatRound.forEach((combatant: Combatant) => {
      if (combatant.id == id) combatant.freeAction1 = value;
    });
  }
  updateCombatantFreeAction2(value: string, id: number) {
    this.announcementRound.forEach((combatant: Combatant) => {
      if (combatant.id == id) combatant.freeAction2 = value;
    });
    this.combatRound.forEach((combatant: Combatant) => {
      if (combatant.id == id) combatant.freeAction2 = value;
    });
  }
  updateAwarenessSelection(value: boolean, id: number) {
    this.announcementRound.forEach((combatant: Combatant) => {
      if (combatant.id == id) combatant.awarenessSelected = value;
    });
  }
}
