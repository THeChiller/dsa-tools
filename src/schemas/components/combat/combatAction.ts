import type { SelectOption } from "naive-ui";

export enum ActionType {
  free,
  regular,
  long,
}
export class Action {
  ////////////////////////////////////////////////
  //               free actions                 //
  ////////////////////////////////////////////////
  public static readonly step = new Action("Schritt", ActionType.free);
  public static readonly activateArtifakt = new Action(
    "Artefakt aktivieren",
    ActionType.free
  );
  public static readonly evade = new Action("Ausweichen", ActionType.free);
  public static readonly passingStrike = new Action(
    "Passierschlag",
    ActionType.free
  );
  public static readonly call = new Action("Rufen", ActionType.free);
  public static readonly throwToTheGround = new Action(
    "zu Boden werfen",
    ActionType.free
  );
  public static readonly drop = new Action(
    "etwas fallen lassen",
    ActionType.free
  );
  public static readonly rotate = new Action("Drehen", ActionType.free);

  ////////////////////////////////////////////////
  //              regular actions               //
  ////////////////////////////////////////////////
  public static readonly attack = new Action(
    "Angriffsaktion",
    ActionType.regular
  );
  public static readonly defense = new Action(
    "Abwehraktion",
    ActionType.regular
  );
  public static readonly move = new Action("Bewegen", ActionType.regular);
  public static readonly position = new Action("Position", ActionType.regular);
  public static readonly tactic = new Action("Taktik", ActionType.regular);
  public static readonly breathe = new Action("Atem holen", ActionType.regular);

  ////////////////////////////////////////////////
  //               long actions                 //
  ////////////////////////////////////////////////
  public static readonly use = new Action(
    "Gegenstand benutzen",
    ActionType.long
  );
  public static readonly reload = new Action("Nachladen", ActionType.long);
  public static readonly orientate = new Action(
    "Orientierung",
    ActionType.long
  );
  public static readonly sprint = new Action("Sprinten", ActionType.long);
  public static readonly talent = new Action("Talenteinsatz", ActionType.long);
  public static readonly draw = new Action("Waffe ziehen", ActionType.long);
  public static readonly cast = new Action("Zaubern", ActionType.long);

  private constructor(
    public readonly name: string,
    public readonly type: ActionType
  ) {}
}

export type ActionObjectType = {
  name: string;
  type: ActionType;
  duration?: number;
  diceModifier?: number;
  iniMalus?: number;
};

export function getActionObject(
  acceptedActionTypes: Array<ActionType> = [
    ActionType.free,
    ActionType.regular,
    ActionType.long,
  ]
) {
  const obj: Record<string, ActionObjectType> = {};
  for (const ActionKey in Action) {
    // @ts-ignore:next-line
    if (acceptedActionTypes.includes(Action[ActionKey].type))
      // @ts-ignore:next-line
      obj[Action[ActionKey].name] = {
        // @ts-ignore:next-line
        name: Action[ActionKey].name,
        // @ts-ignore:next-line
        type: Action[ActionKey].type,
      };
  }
  return obj;
}

export function getActionObjectFromAction(action: Action): ActionObjectType {
  return getActionObject()[action.name];
}

export function getNSelectOptionsFromAction(
  acceptedActionTypes?: Array<ActionType>
): SelectOption[] {
  const obj = getActionObject(acceptedActionTypes);
  const array: SelectOption[] = [];
  Object.keys(obj).forEach((key) => {
    array.push({
      label: key,
      value: key,
    });
  });
  return array;
}
