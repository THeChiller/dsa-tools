.DEFAULT_GOAL := build
.PHONY: clean install_node_modules install build lint reformat

clean :
	rm -rf dist

install_node_modules:
	yarn install

install: install_node_modules

build:
	yarn run build-fragile

lint:
	yarn run lint

reformat: lint
